import React from "react";
import { UserOutlined, RiseOutlined } from "@ant-design/icons";
import styled from "styled-components";

const AuthorsBlock = ({ followers, ideas }) => {
  return (
    <Wrapper>
      <ItemWrapper>
        <UserOutlined style={{ fontSize: "20px" }} />
        <Text>{followers}</Text>
      </ItemWrapper>
      <ItemWrapper>
        <RiseOutlined style={{ fontSize: "20px" }} />
        <Text>{ideas}</Text>
      </ItemWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
`;

const ItemWrapper = styled.div`
  margin-right: 10px;
  padding: 2px;
  display: flex;
`;

const Text = styled.div`
  padding-left: 10px;
  font-size: 14px;
  font-weight: 400;
`;

export default AuthorsBlock;
