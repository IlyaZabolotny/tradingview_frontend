import React, { useState } from "react";
import { Image, List, Avatar } from "antd";
import {
  LikeOutlined,
  HeartOutlined,
  LikeFilled,
  HeartFilled,
} from "@ant-design/icons";
import styled from "styled-components";
import moment from "moment";
import IdeaWatcherFormContainer from "../../modules/idea-watcher-form/containers/idea-watcher-form-container";

const ContentList = ({
  userId,
  setLike,
  removeLike,
  setFavoriteIdea,
  removeFavoriteIdea,
  data,
  authorized,
  pageSize,
  total,
  onChange,
  pageNumber,
  type = "ideas",
}) => {
  const [ideaModalVisible, setIdeaModalVisible] = useState(false);
  const [activeItem, setActiveItem] = useState(null);
  return (
    <>
      <IdeaWatcherFormContainer
        visible={ideaModalVisible}
        setVisible={setIdeaModalVisible}
        data={activeItem}
      />
      <StyledList
        itemLayout="vertical"
        pagination={{
          onChange,
          pageSize,
          total,
          current: pageNumber,
        }}
        authorized={authorized}
        dataSource={data}
        renderItem={(item) => (
          <List.Item
            key={item.id}
            extra={
              <Image
                src={`data:image/jpeg;base64,${item.image}`}
                preview={false}
                width={300}
                height={150}
              />
            }
          >
            <StyledListItemMeta
              avatar={<Avatar src={`data:image/jpeg;base64,${item.avatar}`} />}
              title={item.login}
              description={moment(item.created).format("MMM Do")}
            />
            <ContentWrapper>
              <StyledTitle
                onClick={() => {
                  setActiveItem(item);
                  setIdeaModalVisible(true);
                }}
              >
                {item.title}
              </StyledTitle>
              {item.content}
            </ContentWrapper>
            {authorized ? (
              <ActionWrapper>
                {item.liked === 0 ? (
                  <ItemWrapper>
                    <StyledLikeOutlined
                      onClick={() => {
                        let searchValue = "";
                        if (type === "ideas") {
                          searchValue =
                            document.getElementById("search-ideas").value;
                        } else {
                          searchValue = document.getElementById(
                            "favorite-ideas-search"
                          ).value;
                        }
                        setLike(userId, item.id, pageNumber, searchValue);
                      }}
                    />
                    {item.likesCount}
                  </ItemWrapper>
                ) : (
                  <ItemWrapper>
                    <StyledLikeFilled
                      onClick={() => {
                        let searchValue = "";
                        if (type === "ideas") {
                          searchValue =
                            document.getElementById("search-ideas").value;
                        } else {
                          searchValue = document.getElementById(
                            "favorite-ideas-search"
                          ).value;
                        }
                        removeLike(userId, item.id, pageNumber, searchValue);
                      }}
                    />
                    {item.likesCount}
                  </ItemWrapper>
                )}
                {item.addedToFav === 0 ? (
                  <ItemWrapper>
                    <HeartOutlined
                      onClick={() => {
                        let searchValue = "";
                        if (type === "ideas") {
                          searchValue =
                            document.getElementById("search-ideas").value;
                        } else {
                          searchValue = document.getElementById(
                            "favorite-ideas-search"
                          ).value;
                        }
                        setFavoriteIdea(
                          userId,
                          item.id,
                          pageNumber,
                          searchValue
                        );
                      }}
                    />
                  </ItemWrapper>
                ) : (
                  <ItemWrapper>
                    <StyledHeartFilled
                      onClick={() => {
                        let searchValue = "";
                        if (type === "ideas") {
                          searchValue =
                            document.getElementById("search-ideas").value;
                        } else {
                          searchValue = document.getElementById(
                            "favorite-ideas-search"
                          ).value;
                        }
                        removeFavoriteIdea(
                          userId,
                          item.id,
                          pageNumber,
                          searchValue
                        );
                      }}
                    />
                  </ItemWrapper>
                )}
              </ActionWrapper>
            ) : null}
          </List.Item>
        )}
      />
    </>
  );
};

const StyledList = styled(List)`
  background: white;
  padding: 10px;
  font-size: 16px;
  font-family: "Noto Sans", serif;
  border: 1px solid #e0e3eb;
  border-radius: 5px;
`;

const ItemWrapper = styled.div`
  padding-right: 10px;
`;

const ContentWrapper = styled.div`
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
`;

const ActionWrapper = styled.div`
  margin: 5px 0;
  display: flex;
`;

const StyledLikeOutlined = styled(LikeOutlined)`
  margin-right: 5px;
`;

const StyledLikeFilled = styled(LikeFilled)`
  margin-right: 5px;
  color: #2196f3;
`;

const StyledHeartFilled = styled(HeartFilled)`
  color: #2196f3;
`;

const StyledListItemMeta = styled(List.Item.Meta)`
  .ant-list-item-meta-title {
    font-size: 25px;
    font-family: "Noto Sans", serif;
  }
  .ant-list-item-meta-description {
    font-size: 14px;
    font-family: "Noto Sans", serif;
    color: #787b86;
  }
`;

const StyledTitle = styled.div`
  font-weight: bold;
  cursor: pointer;
`;

export default ContentList;
