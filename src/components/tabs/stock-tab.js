import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import { Table, Input } from "antd";

const { Search } = Input;

const columns = [
  {
    title: "TICKER",
    dataIndex: "symbol",
    key: "ticker",
    render: (symbol) => <Link to={`stock/${symbol}`}>{symbol}</Link>,
  },
  {
    title: "COMPANY",
    dataIndex: "companyName",
    key: "company",
  },
  {
    title: "SECTOR",
    dataIndex: "sector",
    key: "sector",
  },
  {
    title: "BETA",
    dataIndex: "beta",
    key: "beta",
  },
  {
    title: "LAST",
    dataIndex: "price",
    key: "last",
  },
  {
    title: "VOL",
    dataIndex: "volume",
    key: "vol",
  },
  {
    title: "MKT CAP",
    dataIndex: "marketCap",
    key: "mkt cap",
  },
  {
    title: "DIVIDEND",
    dataIndex: "lastAnnualDividend",
    key: "dividend",
  },
];

const StockTab = ({
  data,
  pageSize,
  pageNumber,
  total,
  onChange,
  onSearch,
  loading,
}) => {
  return (
    <StocksWrapper>
      <StyledSearch
        id="search-stocks"
        placeholder="Sector"
        allowClear
        onSearch={onSearch}
      />
      <StyledTable
        rowKey={() => uuidv4()}
        columns={columns}
        dataSource={data}
        loading={loading}
        pagination={{
          onChange,
          pageSize,
          total,
          current: pageNumber,
        }}
        align="center"
      />
    </StocksWrapper>
  );
};

const StocksWrapper = styled.div`
  margin: 30px 0;
`;

const StyledTable = styled(Table)`
  font-family: "Noto Sans", serif;
`;

const StyledSearch = styled(Search)`
  width: 20%;
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-size: 18px;
  margin-bottom: 26px;
`;

export default StockTab;
