import React from "react";
import styled from "styled-components";
import IdeasListContainer from "../../modules/ideas-list/containers/ideas-list-container";
import UsersListContainer from "../../modules/users-list/containers/users-list-container";

const IdeaTab = () => {
  return (
    <Wrapper>
      <IdeasListContainer pageSize={3} />
      <AuthorsWrapper>
        <StyledTitle>Popular authors</StyledTitle>
        <UsersListContainer />
      </AuthorsWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
  margin-top: 30px;
`;

const AuthorsWrapper = styled.div`
  flex-basis: 40%;
  padding-left: 10px;
`;

const StyledTitle = styled.h2`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 500;
  color: #131722;
  margin-bottom: 10px;
`;

export default IdeaTab;
