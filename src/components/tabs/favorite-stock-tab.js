import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import { Table } from "antd";

const columns = [
  {
    title: "TICKER",
    dataIndex: "symbol",
    key: "ticker",
    render: (symbol) => <Link to={`stock/${symbol}`}>{symbol}</Link>,
  },
  {
    title: "COMPANY",
    dataIndex: "name",
    key: "company",
  },
  {
    title: "LAST",
    dataIndex: "price",
    key: "price",
  },
  {
    title: "CHG %",
    dataIndex: "changePercent",
    key: "changePercent",
  },
  {
    title: "CHG",
    dataIndex: "change",
    key: "change",
  },
  {
    title: "VOL",
    dataIndex: "volume",
    key: "volume",
  },
  {
    title: "MKT CAP",
    dataIndex: "marketCapitalization",
    key: "marketCapitalization",
  },
  {
    title: "P/E",
    dataIndex: "priceEarningsRatio",
    key: "priceEarningsRatio",
  },
  {
    title: "EPS",
    dataIndex: "earningsPerShare",
    key: "earningsPerShare",
  },
];

const FavoriteStockTab = ({ data, loading }) => {
  return (
    <StocksWrapper>
      <StyledTable
        rowKey={() => uuidv4()}
        pagination={{ defaultPageSize: 12 }}
        columns={columns}
        dataSource={data}
        loading={loading}
        align="center"
      />
    </StocksWrapper>
  );
};

const StocksWrapper = styled.div`
  margin: 30px 0;
`;

const StyledTable = styled(Table)`
  font-family: "Noto Sans", serif;
`;

export default FavoriteStockTab;
