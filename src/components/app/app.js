import React, { useEffect, useState } from "react";
import { Route, Switch } from "react-router-dom";

import "./app.css";
import Cookies from "universal-cookie";
import MainPageContainer from "../../modules/main-page/containers/main-page-container";
import LoginPageContainer from "../../modules/login-page/containers/login-page-container";
import SignupPageContainer from "../../modules/signup-page/containers/signup-page-container";
import UserPageContainer from "../../modules/user-page/containers/user-page-container";
import StockPageContainer from "../../modules/stcok-page/containers/stock-page-container";
import { connect } from "../../services/web-socket-service";

const App = () => {
  const [userId, setUserId] = useState(null);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setUserId(cookies.get("user_id"));
    }
  }, []);

  useEffect(() => {
    if (userId) {
      connect();
    }
  }, [userId]);
  return (
    <Switch>
      <Route path="/" component={LoginPageContainer} exact />
      <Route path="/main" component={MainPageContainer} exact />
      <Route path="/signup" component={SignupPageContainer} exact />
      <Route path="/user/:userId" component={UserPageContainer} exact />
      <Route path="/stock/:symbol" exact component={StockPageContainer} />
    </Switch>
  );
};

export default App;
