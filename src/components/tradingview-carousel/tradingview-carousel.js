import React from "react";
import { Carousel, Image } from "antd";

import styled from "styled-components";

import Image1 from "../imgs/loginImg1.jpg";
import Image2 from "../imgs/loginImg2.jpg";
import Image3 from "../imgs/loginImg3.jpg";

const TradingviewCarousel = () => {
  return (
    <Carousel autoplay speed={1000}>
      <StyledImage src={Image1} preview={false} />
      <StyledImage src={Image2} preview={false} />
      <StyledImage src={Image3} preview={false} />
    </Carousel>
  );
};

const StyledImage = styled(Image)`
  height: 100vh;
`;

export default TradingviewCarousel;
