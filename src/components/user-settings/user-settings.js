import React from "react";
import styled from "styled-components";
import { Form, Input, Button, Upload, message, Modal } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import Service from "../../services/service";

const validateMessages = {
  // eslint-disable-next-line no-template-curly-in-string
  required: "${label} is required!",
  types: {
    // eslint-disable-next-line no-template-curly-in-string
    email: "${label} is not a valid email!",
    // eslint-disable-next-line no-template-curly-in-string
    number: "${label} is not a valid number!",
  },
  number: {
    // eslint-disable-next-line no-template-curly-in-string
    range: "${label} must be between ${min} and ${max}",
  },
};

const props = {
  name: "file",
  action: "https://www.mocky.io/v2/5cc8019d300000980a055e76",
  headers: {
    authorization: "authorization-text",
  },
  onChange(info) {
    if (info.file.status !== "uploading") {
      console.log(info.file, info.fileList);
    }
    if (info.file.status === "done") {
      message.success(`${info.file.name} file uploaded successfully`);
    } else if (info.file.status === "error") {
      message.error(`${info.file.name} file upload failed.`);
    }
  },
};

function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  return isJpgOrPng;
}

const encodeFileBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    fileReader.onload = () => {
      resolve(fileReader.result);
    };
    fileReader.onerror = (error) => {
      reject(error);
    };
  });
};

const FormStyled = styled(Form)`
  font-style: normal;
  font-family: "Noto Sans", serif;
  .ant-form-item-required {
    font-size: 16px;
    color: #787b86;
  }
  .ant-form-item-label label {
    font-size: 16px;
    color: #787b86;
  }
`;

const FormItemStyled = styled(Form.Item)`
  padding-bottom: 10px;
`;

const InputPasswordStyled = styled(Input.Password)`
  height: 30px;
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 18px;
  font-style: normal;
`;

const InputStyled = styled(Input)`
  height: 30px;
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 14px;
  font-style: normal;
  font-family: "Noto Sans", serif;
`;

const UserSettings = ({ visible, onCancel, setVisible, userData }) => {
  const [form] = Form.useForm();

  const onFinish = async (values) => {
    const result = {};
    result.title = values.title;
    result.content = values.content;
    const image = await encodeFileBase64(values.upload.file.originFileObj);
    result.image = image.substring(image.indexOf(",") + 1);
    const service = new Service();
    service
      .publish(result)
      .then((response) => {
        console.log(response.data);
      })
      .catch((err) => {
        console.log(err);
      });
    setVisible(false);
  };

  return (
    <Modal
      visible={visible}
      centered
      title="Settings"
      okText="Save settings"
      onCancel={onCancel}
      onOk={form.submit}
      width={670}
    >
      <FormStyled
        layout="vertical"
        name="user-settings"
        onFinish={onFinish}
        validateMessages={validateMessages}
        labelAlign="left"
        afterCancel={form.resetFields()}
        form={form}
      >
        <FormItemStyled
          name="login"
          label="Login"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <InputStyled placeholder={userData.login} disabled />
        </FormItemStyled>
        <FormItemStyled
          name="email"
          label="Email"
          rules={[
            {
              type: "email",
            },
            {
              required: true,
            },
          ]}
        >
          <InputStyled />
        </FormItemStyled>
        <FormItemStyled
          name="password"
          label="Password"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <InputPasswordStyled />
        </FormItemStyled>
        <FormItemStyled name="firstname" label="First name">
          <InputStyled />
        </FormItemStyled>
        <FormItemStyled name="lastname" label="Last name">
          <InputStyled />
        </FormItemStyled>
        <FormItemStyled name="company" label="Company">
          <InputStyled />
        </FormItemStyled>
        <Form.Item name="upload" label="Avatar">
          <Upload
            {...props}
            method="GET"
            name="avatar"
            beforeUpload={beforeUpload}
            maxCount={1}
          >
            <Button icon={<UploadOutlined />}>Upload photo</Button>
          </Upload>
        </Form.Item>
      </FormStyled>
    </Modal>
  );
};

export default UserSettings;
