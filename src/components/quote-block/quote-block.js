import React from "react";
import styled from "styled-components";

const QuoteBlock = ({ price, changes, changesPercentage }) => {
  return (
    <Wrapper>
      <PriceWrapper>
        <Text>{price}</Text>
      </PriceWrapper>
      <ChangeWrapper>
        {changes >= 0 ? (
          <div>
            <ItemWrapper>
              <UpText>{changes}</UpText>
            </ItemWrapper>
            <ItemWrapper>
              <UpText>{changesPercentage}</UpText>
            </ItemWrapper>
          </div>
        ) : (
          <div>
            <ItemWrapper>
              <DownText>{changes}</DownText>
            </ItemWrapper>
            <ItemWrapper>
              <DownText>{changesPercentage}</DownText>
            </ItemWrapper>
          </div>
        )}
      </ChangeWrapper>
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
`;

const ItemWrapper = styled.div`
  width: 100%;
  display: flex;
`;

const PriceWrapper = styled.div`
  margin-top: 15px;
`;

const Text = styled.div`
  padding-left: 10px;
  font-size: 15px;
  color: #131722;
`;

const UpText = styled.div`
  padding-left: 10px;
  font-size: 15px;
  color: #26a69a;
`;

const DownText = styled.div`
  padding-left: 10px;
  font-size: 15px;
  color: #ef5350;
`;

const ChangeWrapper = styled.div`
  padding: 0 0 0 70px;
`;

export default QuoteBlock;
