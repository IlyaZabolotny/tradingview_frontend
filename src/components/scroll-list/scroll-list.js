import React from "react";
import { List, Avatar, Button } from "antd";
import styled from "styled-components";
import AuthorsBlock from "../authors-block/authors-block";

const ScrollList = (authorized, userId, loadMore, data) => {
  return (
    <StyledList
      loadMore={loadMore}
      dataSource={data}
      renderItem={(item) => (
        <List.Item
          key={item.id}
          extra={authorized && <StyledButton>Follow</StyledButton>}
        >
          <StyledListItemMeta
            avatar={
              <Avatar
                shape="square"
                src={`data:image/jpeg;base64,${item.avatar}`}
              />
            }
            title={<a href="https://ant.design">{item.login}</a>}
            description={
              <AuthorsBlock followers={item.followers} ideas={item.ideas} />
            }
          />
        </List.Item>
      )}
    />
  );
};

const StyledList = styled(List)`
  background: white;
  border: 1px solid #e0e3eb;
  border-radius: 5px;
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 500;
  line-height: 30px;
  overflow: auto;
  height: 673px;
  .ant-list-item-meta-title {
    font-size: 16px;
  }
  padding: 0 10px;
`;

const StyledButton = styled(Button)`
  background: #ffffff;
  border: 1px solid #38a3f3;
  box-sizing: border-box;
  border-radius: 5px;
  color: #38a3f3;
  font-family: "Noto Sans", serif;
  width: 200px;
`;

const StyledListItemMeta = styled(List.Item.Meta)`
  .ant-avatar {
    margin-top: 12px;
  }
`;

export default ScrollList;
