import React from "react";
import {
  PageHeader,
  Button,
  Input,
  Image,
  Menu,
  Avatar,
  Tooltip,
  AutoComplete,
} from "antd";
import styled from "styled-components";

import { Link } from "react-router-dom";
import { UserOutlined } from "@ant-design/icons";
import Image1 from "../imgs/compLogo.png";

const { Search } = Input;
const { SubMenu } = Menu;

const TradingviewHeader = ({
  authorized,
  logOut,
  userName,
  userId,
  footer,
  onLoginClick,
  onRegisterClick,
  avatar,
}) => {
  const extraElements = authorized
    ? [
        <StyledMenu
          key="menu"
          mode="horizontal"
          triggerSubMenuAction="click"
          onClick={(e) => {
            if (e.key === "sign out") {
              logOut(userId);
            }
          }}
        >
          <StyledSubMenu
            key="subMenu"
            title={
              <Tooltip placement="bottomLeft" title={userName}>
                {userName}
              </Tooltip>
            }
            icon={
              <Avatar
                shape="square"
                size="large"
                src={`data:image/jpeg;base64,${avatar}`}
                icon={<UserOutlined />}
              />
            }
          >
            <StyledMenuItem key="profile">
              <Link to="/user">Profile</Link>
            </StyledMenuItem>
            <StyledMenuItem key="sign out">
              <Link to="/">Sign Out</Link>
            </StyledMenuItem>
          </StyledSubMenu>
        </StyledMenu>,
      ]
    : [
        <StyledButton key="loginBtn" type="text" onClick={onLoginClick}>
          Login
        </StyledButton>,
        <StyledButton
          key="RegisterBtn"
          type="primary"
          onClick={onRegisterClick}
        >
          Register
        </StyledButton>,
      ];

  return (
    <StyledPageHeader
      className="site-page-header-responsive"
      title={<StyledHeaderTitle to="/main">TradingView</StyledHeaderTitle>}
      avatar={{ src: <Image src={Image1} preview={false} /> }}
      extra={extraElements}
      footer={footer}
      tags={
        <AutoComplete>
          <SearchWrapper>
            <StyledSearch
              addonBefore={<StyledAddonBefore>Ticker</StyledAddonBefore>}
              placeholder="Search"
              allowClear
            />
          </SearchWrapper>
        </AutoComplete>
      }
    />
  );
};

const StyledPageHeader = styled(PageHeader)`
  margin: 0 64px;
  padding: 10px 0;
  font-family: "Noto Sans", serif;
  .ant-page-header-heading-title {
    font-size: 18px;
    font-weight: 400;
    font-color: #131722;
  }
`;

const StyledAddonBefore = styled.div`
  font-size: 14px;
  font-family: "Noto Sans", serif;
  color: #131722;
  background: #f0f3fa;
  border: 0;
  height: 32px;
  line-height: 30px;
`;

const StyledSearch = styled(Search)`
  width: 1000px;
  font-size: 18px;
  font-family: "Noto Sans", serif;
  .ant-input-group-addon {
    background: #f0f3fa;
  }
`;

const SearchWrapper = styled.div`
  margin-left: 230px;
  .ant-input-affix-wrapper {
    background: #f0f3fa;
    border: 0;
    height: 32px;
  }
  .ant-input {
    background: #f0f3fa;
  }
  .ant-input-group-addon
    .ant-select.ant-select-single:not(.ant-select-customize-input)
    .ant-select-selector {
    background: #f0f3fa;
    border: 0;
    height: 30px;
  }
  .ant-input-group-addon {
    border: 0;
  }
  .ant-btn.ant-btn-icon-only.ant-input-search-button {
    background: #f0f3fa;
    border: 0;
  }
`;

const StyledButton = styled(Button)`
  font-size: 18px;
  font-family: "Noto Sans", serif;
  border-radius: 5px;
  font-weight: 500;
  line-height: 17px;
  text-align: center;
`;

const StyledMenu = styled(Menu)`
  font-size: 18px;
  font-family: "Noto Sans", serif;
  border: 0;
  .ant-menu-submenu {
    border-style: none !important;
  }
`;

const StyledMenuItem = styled(Menu.Item)`
  font-size: 14px;
  font-family: "Noto Sans", serif;
  width: 200px;
`;

const StyledSubMenu = styled(SubMenu)`
  width: 200px;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

const StyledHeaderTitle = styled(Link)`
  color: #131722;
  margin: 0 12px 0 0;
  &: hover {
    color: #131722;
  }
`;

export default TradingviewHeader;
