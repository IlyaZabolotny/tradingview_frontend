import * as actionTypes from "../types/types";

const initialState = {
  idea: null,
};

const patchIdeaFormReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PATCH_IDEA_SUCCESS:
      return {
        ...state,
        idea: action.idea,
      };
    default:
      return state;
  }
};

export default patchIdeaFormReducer;
