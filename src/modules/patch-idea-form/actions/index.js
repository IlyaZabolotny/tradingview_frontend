import * as types from "../types/types";

export const patchIdea = (values, userId, ideaId, updateList) => {
  return {
    type: types.PATCH_IDEA_REQUEST,
    values,
    userId,
    ideaId,
    updateList,
  };
};

export const ideaPatched = (idea) => {
  return {
    type: types.PATCH_IDEA_SUCCESS,
    idea,
  };
};
