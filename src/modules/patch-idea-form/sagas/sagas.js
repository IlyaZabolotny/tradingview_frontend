import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { ideaCreated } from "../actions";
import * as actionTypes from "../types/types";
import { loadProfile } from "../../profile-bio/actions";

// import { sendMessage } from "../../../services/web-socket-service";

const encodeFileBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    fileReader.onload = () => {
      resolve(fileReader.result);
    };
    fileReader.onerror = (error) => {
      reject(error);
    };
  });
};

function* sagaWorker({ values, userId, ideaId }) {
  const result = {};
  result.id = ideaId;
  result.title = values.title;
  result.content = values.content;
  const image = yield encodeFileBase64(values.upload.file.originFileObj);
  result.image = image.substring(image.indexOf(",") + 1);
  result.author = userId;
  const service = new Service();
  try {
    const idea = yield call(service.publish, result);
    yield put(ideaCreated(idea));
  } catch (error) {
    console.error(error);
  }
}

export function* createIdeaWatcher() {
  yield takeLatest(actionTypes.PATCH_IDEA_REQUEST, sagaWorker);
}
