import { connect } from "react-redux";
import PatchIdeaForm from "../components/patch-idea-form";
import { requestIdeas } from "../../user-ideas-list/actions";
import { patchIdea } from "../actions";

const mapDispatchToProps = (dispatch) => ({
  patchIdea: (values, userId, ideaId, updateList) => {
    dispatch(patchIdea(values, userId, ideaId, updateList));
  },
  fetchData: (userId, pageNumber, keyword) => {
    dispatch(requestIdeas(userId, pageNumber, keyword));
  },
});

const PatchIdeaFormContainer = connect(null, mapDispatchToProps)(PatchIdeaForm);

export default PatchIdeaFormContainer;
