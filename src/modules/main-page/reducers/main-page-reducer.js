import * as actionTypes from "../types/types";

const initialState = {
  login: null,
  avatar: null,
};

const mainPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_USERNAME_SUCCESS:
      return {
        ...state,
        login: action.login,
        avatar: action.avatar,
      };
    case actionTypes.USER_LOGOUT_SUCCESS:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default mainPageReducer;
