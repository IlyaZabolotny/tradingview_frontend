import { takeLatest, call, put } from "redux-saga/effects";
import Cookies from "universal-cookie";
import Service from "../../../services/service";
import { loadUserName, userLoggedOut } from "../actions";
import * as actionTypes from "../types/types";
import { disconnect } from "../../../services/web-socket-service";

const cookies = new Cookies();

function* sagaWorker({ userId }) {
  const service = new Service();
  try {
    const payload = yield call(service.getUser, userId);
    yield put(loadUserName(payload.data.login, payload.data.avatar));
  } catch (error) {
    console.log(error);
  }
}

function* sagaLogoutWorker({ userId }) {
  const service = new Service();
  try {
    yield call(service.logout, userId);
    cookies.remove("token");
    cookies.remove("user_id");
    disconnect();
    yield put(userLoggedOut());
  } catch (error) {
    console.log(error);
  }
}

export function* mainPageWatcher() {
  yield takeLatest(actionTypes.FETCH_USERNAME_REQUEST, sagaWorker);
}

export function* signOutWatcher() {
  yield takeLatest(actionTypes.USER_LOGOUT_REQUEST, sagaLogoutWorker);
}
