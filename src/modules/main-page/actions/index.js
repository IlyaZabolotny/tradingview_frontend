import * as types from "../types/types";

export const requestUserName = (userId) => {
  return {
    type: types.FETCH_USERNAME_REQUEST,
    userId,
  };
};

export const loadUserName = (login, avatar) => {
  return {
    type: types.FETCH_USERNAME_SUCCESS,
    login,
    avatar,
  };
};

export const userLogOut = (userId) => {
  return {
    type: types.USER_LOGOUT_REQUEST,
    userId,
  };
};

export const userLoggedOut = () => {
  return {
    type: types.USER_LOGOUT_SUCCESS,
  };
};
