import { connect } from "react-redux";
import { requestUserName, userLogOut } from "../actions";
import MainPage from "../components/main-page";

const mapStateToProps = (state) => {
  return {
    login: state.mainPage.login,
    avatar: state.mainPage.avatar,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getUserName: (userId) => {
    dispatch(requestUserName(userId));
  },
  logOut: (userId) => {
    dispatch(userLogOut(userId));
  },
});

const MainPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MainPage);

export default MainPageContainer;
