import React from "react";
import styled from "styled-components";
import { Tabs } from "antd";
import { IdeaTab } from "../../../components/tabs";
import StocksTableContainer from "../../stocks-table/containers/stocks-table-container";
import MarketPageContainer from "../../market-page/containers/market-page-container";
import FavoriteStocksTableContainer from "../../favorite-stocks-table/containers/favorite-stocks-table-container";

const HeaderFooter = ({ authorized }) => {
  return (
    <div>
      {authorized ? (
        <StyledTabs>
          <StyledTabPane tab="Ideas" key="ideasTab">
            <HeaderBorder />
            <IdeaTab />
          </StyledTabPane>
          <StyledTabPane tab="Markets" key="marketsTab">
            <HeaderBorder />
            <MarketPageContainer />
          </StyledTabPane>
          <StyledTabPane tab="Stocks" key="stocksTab">
            <HeaderBorder />
            <StocksTableContainer />
          </StyledTabPane>
          <StyledTabPane tab="Favorite Stocks" key="favoriteStocksTab">
            <HeaderBorder />
            <FavoriteStocksTableContainer />
          </StyledTabPane>
        </StyledTabs>
      ) : (
        <StyledTabs>
          <StyledTabPane tab="Ideas" key="ideasTab">
            <HeaderBorder />
            <IdeaTab />
          </StyledTabPane>
          <StyledTabPane tab="Markets" key="marketsTab">
            <HeaderBorder />
            <MarketPageContainer />
          </StyledTabPane>
          <StyledTabPane tab="Stocks" key="stocksTab">
            <HeaderBorder />
            <StocksTableContainer />
          </StyledTabPane>
        </StyledTabs>
      )}
    </div>
  );
};

const { TabPane } = Tabs;

const StyledTabs = styled(Tabs)`
  border-top: 1px solid #e0e3eb;
  .ant-tabs-nav-list {
    padding-top: 5px;
    padding-bottom: 5px;
  }
  .ant-tabs-tab-btn {
    font-family: "Noto Sans", serif;
  }
`;

const StyledTabPane = styled(TabPane)`
    font-size: 20px;
    1px solid #E0E3EB;
`;

const HeaderBorder = styled.div`
  border-bottom: 1px solid #e0e3eb;
`;

export default HeaderFooter;
