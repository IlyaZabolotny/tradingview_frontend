import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import Cookies from "universal-cookie";
import HeaderFooter from "./header-footer";
import MainTradingViewHeaderContainer from "../../main-trading-view-header/containers/main-trading-view-header-container";

const MainPage = ({ logOut, login, avatar, getUserName }) => {
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const history = useHistory();

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      // eslint-disable-next-line radix
      setUserId(cookies.get("user_id"));
    }
  }, []);

  useEffect(() => {
    if (authorized) {
      getUserName(userId);
    }
  }, [authorized]);

  const onLoginClick = () => {
    history.push("/");
  };

  const onRegisterClick = () => {
    history.push("/signup");
  };

  return (
    <MainTradingViewHeaderContainer
      authorized={authorized}
      avatar={avatar}
      userName={login}
      userId={userId}
      onLoginClick={onLoginClick}
      onRegisterClick={onRegisterClick}
      logOut={logOut}
      footer={<HeaderFooter authorized={authorized} />}
    />
  );
};

export default MainPage;
