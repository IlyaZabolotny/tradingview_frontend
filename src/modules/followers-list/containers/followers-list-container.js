import { connect } from "react-redux";
import { requestFollowers } from "../actions";
import FollowersList from "../components/followers-list";

const mapStateToProps = (state) => {
  const { followers } = state;
  return {
    followers: followers.followers,
    pageNumber: followers.pageNumber,
    total: followers.total,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getFollowers: (userId, pageNumber, keyword) => {
      dispatch(requestFollowers(userId, pageNumber, keyword));
    },
  };
};

const FollowersListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FollowersList);

export default FollowersListContainer;
