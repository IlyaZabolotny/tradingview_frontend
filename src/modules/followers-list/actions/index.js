import * as types from "../types/types";

export const loadFollowers = (followers, pageNumber, total, keyword) => {
  return {
    type: types.FETCH_FOLLOWERS_SUCCESS,
    followers,
    pageNumber,
    total,
    keyword,
  };
};

export const requestFollowers = (userId, pageNumber, keyword) => {
  return {
    type: types.FETCH_FOLLOWERS_REQUEST,
    userId,
    pageNumber,
    keyword,
  };
};
