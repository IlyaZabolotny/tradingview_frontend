import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { loadFollowers } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ userId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.keyword = keyword;
    result.pageNumber = pageNumber - 1;
    result.size = 5;
    const payload = yield call(service.getFollowers, result);
    yield put(
      loadFollowers(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

export function* followersWatcher() {
  yield takeLatest(actionTypes.FETCH_FOLLOWERS_REQUEST, sagaWorker);
}
