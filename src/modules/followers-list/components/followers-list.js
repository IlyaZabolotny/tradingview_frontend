import React, { useEffect, useState } from "react";
import { Avatar, Input, List } from "antd";
import styled from "styled-components";
import moment from "moment";
import Cookies from "universal-cookie";
import FollowersBlockContainer from "../../followers-block/containers/followers-block-container";

// eslint-disable-next-line consistent-return
const itemStatus = (item) => {
  if (item.lastSeenTime === null) {
    return <div style={{ color: "#009688" }}>Online</div>;
  }
  return `Last visit ${moment(item.lastSeenTime).fromNow()}`;
};

const FollowersList = ({ getFollowers, followers, total, anotherUserId }) => {
  const [page, setPage] = useState(1);
  const [anotherUser, setAnotherUser] = useState(false);
  const [userId, setUserId] = useState(null);
  // eslint-disable-next-line no-unused-vars

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      // eslint-disable-next-line radix
      setUserId(cookies.get("user_id"));
    }
  }, []);

  useEffect(() => {
    if (userId) {
      if (userId !== anotherUserId) {
        getFollowers(anotherUserId, 1, "");
        setAnotherUser(true);
        setUserId(anotherUserId);
      } else {
        getFollowers(userId, 1, "");
      }
    }
  }, [userId]);

  const setCurrentPage = (pageNumber) => {
    const searchValue = document.getElementById("followers-list").value;
    getFollowers(userId, pageNumber, searchValue);
    setPage(pageNumber);
  };

  return (
    <Wrapper>
      <StyledSearch
        id="followers-list"
        placeholder="Search authors"
        onSearch={(value) => {
          getFollowers(userId, 1, value);
          setPage(1);
        }}
      />
      <StyledList
        split
        itemLayout="horizontal"
        pagination={{
          onChange: setCurrentPage,
          pageSize: 5,
          total,
          current: page,
        }}
        dataSource={followers}
        bordered
        renderItem={(item) => (
          <List.Item
            extra={
              <FollowersBlockContainer
                users={item.followers}
                ideas={item.ideas}
                userId={userId}
                pageNumber={page}
                subscribedTo={item.id}
                subscribed={item.subscribed}
                anotherUser={anotherUser}
              />
            }
          >
            <List.Item.Meta
              avatar={<Avatar src={`data:image/jpeg;base64,${item.avatar}`} />}
              title={item.login}
              description={itemStatus(item)}
            />
          </List.Item>
        )}
      />
    </Wrapper>
  );
};

const { Search } = Input;

const Wrapper = styled.div`
  padding-top: 20px;
`;

const StyledSearch = styled(Search)`
  margin-bottom: 20px;
  width: 30%;
  font-family: "Noto Sans", serif;
  font-style: normal;
  .ant-input {
    height: 35px;
  }
  .ant-input-search-button {
    height: 35px;
  }
`;

const StyledList = styled(List)`
  background: white;
  padding: 10px;
  border: 1px solid #e0e3eb;
  border-radius: 5px;
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 500;
  font-size: 25px;
  line-height: 30px;
  padding: 10px;
  height: 510px;
  .ant-list-item-meta-title {
    font-size: 18px;
    font-weight: 400;
  }
  .ant-list-item-meta-description {
    font-size: 13px;
  }
`;

export default FollowersList;
