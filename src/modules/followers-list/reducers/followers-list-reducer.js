import * as actionTypes from "../types/types";

const initialState = {
  followers: [],
  pageNumber: 1,
  total: 1,
};

const followersListReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_FOLLOWERS_SUCCESS:
      return {
        ...state,
        followers: action.followers,
        pageNumber: action.pageNumber,
        total: action.total,
      };
    default:
      return state;
  }
};

export default followersListReducer;
