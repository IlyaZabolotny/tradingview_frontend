import * as actionTypes from "../types/types";

const initialState = {
  symbol: null,
  name: null,
  price: 0,
  change: 0,
  changePercent: 0,
  volume: 0,
  marketCapitalization: 0,
  priceEarningsRatio: 0,
  dividendYield: 0,
  earningsPerShare: 0,
  fullTimeEmployees: null,
  earningsAnnouncement: null,
  description: null,
  exchange: null,
  companyLogo: null,
  stockState: false,
  login: null,
  avatar: null,
};

const stockPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_STOCK_SUCCESS:
      return {
        ...state,
        symbol: action.symbol,
        name: action.name,
        price: action.price,
        change: action.change,
        changePercent: action.changePercent,
        volume: action.volume,
        marketCapitalization: action.marketCapitalization,
        priceEarningsRatio: action.priceEarningsRatio,
        dividendYield: action.dividendYield,
        earningsPerShare: action.earningsPerShare,
        fullTimeEmployees: action.fullTimeEmployees,
        earningsAnnouncement: action.earningsAnnouncement,
        description: action.description,
        exchange: action.exchange,
        companyLogo: action.companyLogo,
      };
    case actionTypes.FETCH_USER_SUCCESS:
      return {
        ...state,
        login: action.login,
        avatar: action.avatar,
      };
    case actionTypes.SET_FAVORITE_STOCK_SUCCESS:
      return {
        ...state,
      };
    case actionTypes.REMOVE_FAVORITE_STOCK_SUCCESS:
      return {
        ...state,
      };
    case actionTypes.FETCH_STOCK_STATE_SUCCESS:
      return {
        ...state,
        stockState: action.stockState,
      };
    default:
      return state;
  }
};

export default stockPageReducer;
