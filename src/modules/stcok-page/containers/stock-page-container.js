import { connect } from "react-redux";
import {
  addFavoriteStock,
  removeFavoriteStock,
  requestStock,
  requestStockState,
  requestUser,
} from "../actions";
import StockPage from "../components/stock-page";
import { userLogOut } from "../../main-page/actions";

const mapStateToProps = (state) => {
  const { stock } = state;
  return {
    symbol: stock.symbol,
    name: stock.name,
    price: stock.price,
    change: stock.change,
    changePercent: stock.changePercent,
    volume: stock.volume,
    marketCapitalization: stock.marketCapitalization,
    priceEarningsRatio: stock.priceEarningsRatio,
    dividendYield: stock.dividendYield,
    earningsPerShare: stock.earningsPerShare,
    fullTimeEmployees: stock.fullTimeEmployees,
    earningsAnnouncement: stock.earningsAnnouncement,
    description: stock.description,
    exchange: stock.exchange,
    companyLogo: stock.companyLogo,
    stockState: stock.stockState,
    login: stock.login,
    avatar: stock.avatar,
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchStock: (userId, symbol) => {
    dispatch(requestStock(userId, symbol));
  },
  logOut: (userId, redirectAction) => {
    dispatch(userLogOut(userId, redirectAction));
  },
  addToFav: (userId, symbol) => {
    dispatch(addFavoriteStock(userId, symbol));
  },
  fetchState: (userId, symbol) => {
    dispatch(requestStockState(userId, symbol));
  },
  removeFromFav: (userId, symbol) => {
    dispatch(removeFavoriteStock(userId, symbol));
  },
  getUserData: (userId) => {
    dispatch(requestUser(userId));
  },
});

const StockPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(StockPage);

export default StockPageContainer;
