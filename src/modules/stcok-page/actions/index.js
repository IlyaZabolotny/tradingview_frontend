import * as types from "../types/types";

export const requestStock = (userId, symbol) => {
  return {
    type: types.FETCH_STOCK_REQUEST,
    userId,
    symbol,
  };
};

export const loadStock = (
  symbol,
  name,
  price,
  change,
  changePercent,
  volume,
  marketCapitalization,
  priceEarningsRatio,
  dividendYield,
  earningsPerShare,
  fullTimeEmployees,
  earningsAnnouncement,
  description,
  exchange,
  companyLogo
) => {
  return {
    type: types.FETCH_STOCK_SUCCESS,
    symbol,
    name,
    price,
    change,
    changePercent,
    volume,
    marketCapitalization,
    priceEarningsRatio,
    dividendYield,
    earningsPerShare,
    fullTimeEmployees,
    earningsAnnouncement,
    description,
    exchange,
    companyLogo,
  };
};

export const requestStockState = (userId, symbol) => {
  return {
    type: types.FETCH_STOCK_STATE_REQUEST,
    userId,
    symbol,
  };
};

export const loadStockState = (stockState) => {
  return {
    type: types.FETCH_STOCK_STATE_SUCCESS,
    stockState,
  };
};

export const addFavoriteStock = (userId, symbol) => {
  return {
    type: types.SET_FAVORITE_STOCK_REQUEST,
    userId,
    symbol,
  };
};

export const favoriteStockIsAdded = () => {
  return {
    type: types.SET_FAVORITE_STOCK_SUCCESS,
  };
};

export const removeFavoriteStock = (userId, symbol) => {
  return {
    type: types.REMOVE_FAVORITE_STOCK_REQUEST,
    userId,
    symbol,
  };
};

export const favoriteStockIsRemoved = () => {
  return {
    type: types.REMOVE_FAVORITE_STOCK_SUCCESS,
  };
};

export const requestUser = (userId) => {
  return {
    type: types.FETCH_USER_REQUEST,
    userId,
  };
};

export const loadUser = (login, avatar) => {
  return {
    type: types.FETCH_USER_SUCCESS,
    login,
    avatar,
  };
};
