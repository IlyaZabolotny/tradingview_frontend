import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import * as actionTypes from "../types/types";
import {
  favoriteStockIsAdded,
  favoriteStockIsRemoved,
  loadStock,
  loadStockState,
  loadUser,
} from "../actions";

function* sagaWorker({ symbol }) {
  const service = new Service();
  try {
    const payload = yield call(service.getStock, symbol);
    yield put(
      loadStock(
        payload.data.symbol,
        payload.data.name,
        payload.data.price,
        payload.data.change,
        payload.data.changePercent,
        payload.data.volume,
        payload.data.marketCapitalization,
        payload.data.priceEarningsRatio,
        payload.data.dividendYield,
        payload.data.earningsPerShare,
        payload.data.fullTimeEmployees,
        payload.data.earningsAnnouncement,
        payload.data.description,
        payload.data.exchange,
        payload.data.companyLogo
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* setFavoriteStockWorker({ userId, symbol }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.symbol = symbol;
    yield call(service.addFavoriteStock, result);
    yield put(favoriteStockIsAdded());
    const payload = yield call(service.checkFavoriteStock, result);
    yield put(loadStockState(payload.data));
  } catch (error) {
    console.log(error);
  }
}

function* removeFavoriteStockWorker({ userId, symbol }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.symbol = symbol;
    yield call(service.removeFavoriteStock, result);
    yield put(favoriteStockIsRemoved());
    const payload = yield call(service.checkFavoriteStock, result);
    yield put(loadStockState(payload.data));
  } catch (error) {
    console.log(error);
  }
}

function* stockStateWorker({ userId, symbol }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.symbol = symbol;
    const payload = yield call(service.checkFavoriteStock, result);
    yield put(loadStockState(payload.data));
  } catch (error) {
    console.log(error);
  }
}

export function* stockPageWatcher() {
  yield takeLatest(actionTypes.FETCH_STOCK_REQUEST, sagaWorker);
}

export function* setFavoriteStockWatcher() {
  yield takeLatest(
    actionTypes.SET_FAVORITE_STOCK_REQUEST,
    setFavoriteStockWorker
  );
}

export function* removeFavoriteStockWatcher() {
  yield takeLatest(
    actionTypes.REMOVE_FAVORITE_STOCK_REQUEST,
    removeFavoriteStockWorker
  );
}

function* stockUserWorker({ userId }) {
  const service = new Service();
  try {
    const payload = yield call(service.getUser, userId);
    yield put(loadUser(payload.data.login, payload.data.avatar));
  } catch (error) {
    console.log(error);
  }
}

export function* stockStateWatcher() {
  yield takeLatest(actionTypes.FETCH_STOCK_STATE_REQUEST, stockStateWorker);
}

export function* stockUserWatcher() {
  yield takeLatest(actionTypes.FETCH_USER_REQUEST, stockUserWorker);
}
