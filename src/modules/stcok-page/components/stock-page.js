import React, { useEffect, useState } from "react";
import { Avatar, Statistic, Button, Row, Col } from "antd";
import styled from "styled-components";
import { useHistory } from "react-router-dom";
import moment from "moment";
import Cookies from "universal-cookie";
import StockChartTableContainer from "../../stock-chart-table/containers/stock-chart-table-container";
import IdeaFormContainer from "../../idea-form/containers/idea-form-container";
import MainTradingViewHeaderContainer from "../../main-trading-view-header/containers/main-trading-view-header-container";

const StockPage = ({
  match,
  symbol,
  name,
  price,
  change,
  changePercent,
  marketCapitalization,
  priceEarningsRatio,
  dividendYield,
  earningsPerShare,
  earningsAnnouncement,
  description,
  exchange,
  companyLogo,
  stockState,
  fetchState,
  fetchStock,
  addToFav,
  removeFromFav,
  logOut,
  login,
  avatar,
  getUserData,
}) => {
  const [visible, setVisible] = useState(false);
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      // eslint-disable-next-line radix
      setUserId(cookies.get("user_id"));
    }
  }, []);

  useEffect(() => {
    if (authorized) {
      fetchStock(userId, match.params.symbol);
      fetchState(userId, match.params.symbol);
      // eslint-disable-next-line no-debugger
      debugger;
      getUserData(userId);
    } else {
      fetchStock(userId, match.params.symbol);
    }
  }, [authorized]);

  const history = useHistory();

  const onLoginClick = () => {
    history.push("/");
  };

  const onRegisterClick = () => {
    history.push("/signup");
  };

  return (
    <>
      <MainTradingViewHeaderContainer
        authorized={authorized}
        avatar={avatar}
        userName={login}
        userId={userId}
        onLoginClick={onLoginClick}
        onRegisterClick={onRegisterClick}
        logOut={logOut}
      />
      <MainWrapper>
        <HeaderBorder />
        <Wrapper>
          <StyledAvatar src={companyLogo} shape="circle" />
          <StyledHeader>
            <div>
              <StyledCompanyName>{symbol}</StyledCompanyName>
            </div>
            <Row>
              <Col span={12}>
                <StyledStockName>{name}</StyledStockName>
                <StyledExchangeName>{exchange}</StyledExchangeName>
              </Col>
              <Col span={12} align="right">
                {authorized ? (
                  <div>
                    {!stockState ? (
                      <StyledFavButton
                        onClick={() => {
                          addToFav(userId, symbol);
                        }}
                      >
                        Add to favorites
                      </StyledFavButton>
                    ) : (
                      <StyledRemoveFavButton
                        onClick={() => {
                          removeFromFav(userId, symbol);
                        }}
                      >
                        Remove from favorites
                      </StyledRemoveFavButton>
                    )}
                    <StyledCreateButton onClick={() => setVisible(true)}>
                      Create an idea
                    </StyledCreateButton>
                    <IdeaFormContainer
                      visible={visible}
                      setVisible={setVisible}
                      onCancel={() => history.push(`/user/${userId}`)}
                    />
                  </div>
                ) : null}
              </Col>
            </Row>
          </StyledHeader>
        </Wrapper>
        <PriceWrapper>
          <StyledPriceValue>{price}</StyledPriceValue>
          <StyledPriceCurrency>USD</StyledPriceCurrency>
          {change >= 0 ? (
            <StyledChangeUpValue>
              +{change} (+{changePercent}%)
            </StyledChangeUpValue>
          ) : (
            <StyledChangeDownValue>
              {change}({changePercent}%)
            </StyledChangeDownValue>
          )}
        </PriceWrapper>
        <FundamentalsWrapper>
          <StyledStatistic
            title="UPCOMING EARNINGS"
            value={moment(earningsAnnouncement).format("MMM Do")}
            groupSeparator="."
          />
          <StyledStatistic title="EPS" value={earningsPerShare} precision={2} />
          <StyledStatistic
            title="MARKET CAP"
            value={marketCapitalization}
            groupSeparator="."
          />
          <StyledStatistic
            title="DIV YIELD"
            value={dividendYield}
            precision={2}
            groupSeparator="."
          />
          <StyledStatistic
            title="P/E"
            value={priceEarningsRatio}
            precision={2}
            groupSeparator="."
          />
        </FundamentalsWrapper>
        <StockChartTableContainer symbol={match.params.symbol} />
        <StyledTitle>Description</StyledTitle>
        <StyledDescription>{description}</StyledDescription>
      </MainWrapper>
    </>
  );
};

const MainWrapper = styled.div`
  margin: 0 64px;
`;

const HeaderBorder = styled.div`
  border-bottom: 1px solid #e0e3eb;
`;

const StyledAvatar = styled(Avatar)`
  width: 60px;
  height: 60px;
`;

const Wrapper = styled.div`
  display: flex;
  font-size: 20px;
  font-family: "Noto Sans", serif;
  margin: 20px 0 10px 0;
  width: 100%;
`;

const PriceWrapper = styled.div`
  padding-top: 5px;
  display: flex;
`;

const StyledHeader = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const StyledDescription = styled.span`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 21px;
`;

const FundamentalsWrapper = styled.div`
  display: flex;
  border-bottom: 1px solid #e0e3eb;
  padding-bottom: 10px;
`;

const StyledPriceValue = styled.span`
  color: #131722;
  font-style: normal;
  font-weight: normal;
  font-size: 40px;
  line-height: 49px;
`;

const StyledChangeUpValue = styled.span`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: normal;
  margin-left: 20px;
  font-size: 20px;
  line-height: 65px;
  color: #26a69a;
`;

const StyledChangeDownValue = styled.span`
  font-family: "Noto Sans", serif;
  margin-left: 20px;
  color: #ef5350;
  font-style: normal;
  font-weight: normal;
  font-size: 20px;
  line-height: 65px;
`;

// const ExchangeWrapper = styled.div`
//   display: flex;
//   width: 100%;
// `;

const StyledTitle = styled.h2`
  font-family: "Noto Sans", serif;
  color: #131722;
  font-style: normal;
  font-weight: bold;
  font-size: 20px;
`;

const StyledPriceCurrency = styled.span`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: normal;
  font-size: 18px;
  line-height: 65px;
  letter-spacing: 0.5px;
`;

const StyledCompanyName = styled.span`
  font-family: "Noto Sans", serif;
  margin: 0 10px 0px;
  color: #131722;
  font-weight: bold;
`;

const StyledStockName = styled.span`
  font-family: "Noto Sans", serif;
  margin: 0 10px 0px;
  margin-right: 30px;
  color: #787b86;
  cursor: default;
  font-weight: normal;
`;

const StyledExchangeName = styled.span`
  font-family: "Noto Sans", serif;
  margin: 0;
  color: #787b86;
  cursor: default;
  font-weight: normal;
`;

const StyledFavButton = styled(Button)`
  // margin-left: 100px;
  width: 275px;
  height: 30px;
  font-weight: 600;
  color: #2196f3;
  border: 1px solid #2196f3;
  box-sizing: border-box;
  border-radius: 5px;
  :active {
    border: 1px solid #2196f3;
    color: #2196f3;
  }
  :focus {
    border: 1px solid #2196f3;
    color: #2196f3;
  }
  :hover {
    border: 1px solid #2196f3;
    color: white;
    background: #2196f3;
  }
`;

const StyledRemoveFavButton = styled(Button)`
  // margin-left: 100px;
  width: 275px;
  height: 30px;
  font-weight: 600;
  color: #ef5350;
  border: 1px solid #ef5350;
  box-sizing: border-box;
  border-radius: 5px;
  :active {
    border: 1px solid #ef5350;
    color: #ef5350;
  }
  :focus {
    border: 1px solid #ef5350;
    color: #ef5350;
  }
  :hover {
    border: 1px solid #ef5350;
    color: white;
    background: #ef5350;
  }
`;

const StyledCreateButton = styled(Button)`
  margin-left: 20px;
  width: 275px;
  height: 30px;
  font-weight: 600;
  border: 1px solid #3faa9e;
  box-sizing: border-box;
  border-radius: 5px;
  color: #3faa9e;
  :active {
    border: 1px solid #3faa9e;
    color: #3faa9e;
  }
  :focus {
    border: 1px solid #3faa9e;
    color: #3faa9e;
  }
  :hover {
    border: 1px solid #3faa9e;
    color: white;
    background: #3faa9e;
  }
`;

const StyledStatistic = styled(Statistic)`
  font-family: "Noto Sans", serif;
  margin-right: 63px;
  .ant-statistic-title {
    font-style: normal;
    font-weight: normal;
    font-size: 15px;
    line-height: 18px;
    color: #80838d;
    margin-bottom: 0;
  }
  .ant-statistic-content {
    font-size: 15px;
  }
  .ant-statistic-content-value-int {
    font-family: "Noto Sans", serif;
    font-style: normal;
    font-weight: bold;
    font-size: 15px;
    line-height: 18px;
  }
  .ant-statistic-content-value-decimal {
    font-family: "Noto Sans", serif;
    font-style: normal;
    font-weight: bold;
    font-size: 15px;
    line-height: 18px;
  }
  .ant-statistic-content-value {
    font-family: "Noto Sans", serif;
    font-style: normal;
    font-weight: bold;
    font-size: 15px;
    line-height: 18px;
  }
`;

export default StockPage;
