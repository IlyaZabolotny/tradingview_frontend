import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Tabs } from "antd";
import styled from "styled-components";
import Cookies from "universal-cookie";
import ProfileBioContainer from "../../profile-bio/containers/profile-bio-container";
import UserIdeas from "../../user-ideas/components/user-ideas";
import FollowersListContainer from "../../followers-list/containers/followers-list-container";
import FollowingsListContainer from "../../followings-list/containers/followings-list-container";
import MainTradingViewHeaderContainer from "../../main-trading-view-header/containers/main-trading-view-header-container";

const UserPage = ({ match, getUser, logOut, login, avatar }) => {
  const [userId, setUserId] = useState(null);
  const [anotherUserId, setAnotherUserId] = useState(null);
  const history = useHistory();
  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setUserId(cookies.get("user_id"));
      setAnotherUserId(match.params.userId);
    } else {
      history.push("/");
    }
  }, []);

  useEffect(() => {
    if (userId) {
      getUser(userId);
    }
  }, [userId]);

  return (
    <>
      <MainTradingViewHeaderContainer
        authorized
        userId={userId}
        userName={login}
        avatar={avatar}
        logOut={logOut}
      />
      <ProfileBioContainer anotherUserId={anotherUserId} />
      <div>
        <StyledTabsGeneral defaultActiveKey="1">
          <TabPane tab="IDEAS" key="ideas">
            <UserIdeas anotherUserId={anotherUserId} />
          </TabPane>
          <TabPane tab="FOLLOWERS" key="follow">
            <FollowersListContainer anotherUserId={anotherUserId} />
          </TabPane>
          <TabPane tab="FOLLOWINGS" key="unfollow">
            <FollowingsListContainer anotherUserId={anotherUserId} />
          </TabPane>
        </StyledTabsGeneral>
      </div>
    </>
  );
};

const { TabPane } = Tabs;

const StyledTabsGeneral = styled(Tabs)`
  margin: 0 64px;
  .ant-tabs-nav-list {
    padding: 5px 0;
  }
  .ant-tabs-tab-btn {
    font-family: "Noto Sans", serif;
    font-weight: 800;
    font-size: 14px;
    letter-spacing: 0.12ex;
  }
  .ant-tabs-nav {
    margin: 0;
  }
`;

export default UserPage;
