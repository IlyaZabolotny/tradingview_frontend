import * as types from "../types/types";

export const requestUser = (userId) => {
  return {
    type: types.FETCH_USER_REQUEST,
    userId,
  };
};

export const loadUser = (login, avatar) => {
  return {
    type: types.FETCH_USER_SUCCESS,
    login,
    avatar,
  };
};
