import * as actionTypes from "../types/types";

const initialState = {
  login: null,
  avatar: null,
};

const userPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_USER_SUCCESS:
      return {
        ...state,
        login: action.login,
        avatar: action.avatar,
      };
    default:
      return state;
  }
};

export default userPageReducer;
