import { connect } from "react-redux";
import { requestUser } from "../actions";
import UserPage from "../components/user-page";
import { userLogOut } from "../../main-page/actions";

const mapStateToProps = (state) => {
  return {
    login: state.userPage.login,
    avatar: state.userPage.avatar,
  };
};

const mapDispatchToProps = (dispatch) => ({
  getUser: (userId) => {
    dispatch(requestUser(userId));
  },
  logOut: (userId, redirectAction) => {
    dispatch(userLogOut(userId, redirectAction));
  },
});

const UserPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPage);

export default UserPageContainer;
