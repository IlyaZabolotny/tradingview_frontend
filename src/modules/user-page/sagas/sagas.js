import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import * as actionTypes from "../types/types";
import { loadUser } from "../actions";

function* sagaWorker({ userId }) {
  const service = new Service();
  try {
    const payload = yield call(service.getUser, userId);
    yield put(loadUser(payload.data.login, payload.data.avatar));
  } catch (error) {
    console.log(error);
  }
}
export function* userPageWatcher() {
  yield takeLatest(actionTypes.FETCH_USER_REQUEST, sagaWorker);
}
