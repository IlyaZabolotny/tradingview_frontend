import * as types from "../types/types";

export const editProfile = (userId, values) => {
  return {
    type: types.EDIT_PROFILE_REQUEST,
    userId,
    values,
  };
};

export const profileEdited = (profile) => {
  return {
    type: types.EDIT_PROFILE_SUCCESS,
    profile,
  };
};
