import { connect } from "react-redux";
import UserSettings from "../components/user-settings";
import { editProfile } from "../actions";

const mapDispatchToProps = (dispatch) => ({
  editProfile: (userId, values) => {
    dispatch(editProfile(userId, values));
  },
});

const UserSettingsContainer = connect(null, mapDispatchToProps)(UserSettings);

export default UserSettingsContainer;
