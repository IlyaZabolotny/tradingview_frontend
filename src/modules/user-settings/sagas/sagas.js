import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { profileEdited } from "../actions";
import * as actionTypes from "../types/types";
import { loadProfile } from "../../profile-bio/actions";
import { loadUser } from "../../user-page/actions";
import { ideasLoaded } from "../../user-ideas-list/actions";
// import { sendMessage } from "../../../services/web-socket-service";

const encodeFileBase64 = (file) => {
  return new Promise((resolve, reject) => {
    const fileReader = new FileReader();
    fileReader.readAsDataURL(file);
    fileReader.onload = () => {
      resolve(fileReader.result);
    };
    fileReader.onerror = (error) => {
      reject(error);
    };
  });
};

function* sagaWorker({ userId, values }) {
  const result = {};
  result.id = userId;
  result.email = values.email;
  result.password = values.password;
  result.firstName = values.firstName;
  result.lastName = values.lastName;
  result.company = values.company;
  // eslint-disable-next-line no-debugger
  debugger;
  if (values.upload !== undefined) {
    const image = yield encodeFileBase64(values.upload.file.originFileObj);
    result.avatar = image.substring(image.indexOf(",") + 1);
  } else {
    result.avatar = null;
  }
  const service = new Service();
  try {
    const profile = yield call(service.patchUser, result);
    yield put(profileEdited(profile));
    const profileBioPayload = yield call(service.getProfile, userId);
    yield put(loadProfile(profileBioPayload.data));
    const userPayload = yield call(service.getUser, userId);
    yield put(loadUser(userPayload.data.login, userPayload.data.avatar));
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = "";
    updateResult.pageNumber = 0;
    updateResult.size = 2;
    const payload = yield call(service.getAuthorIdeas, updateResult);
    yield put(
      ideasLoaded(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        ""
      )
    );
  } catch (error) {
    console.error(error);
  }
}

export function* editUserProfileWatcher() {
  yield takeLatest(actionTypes.EDIT_PROFILE_REQUEST, sagaWorker);
}
