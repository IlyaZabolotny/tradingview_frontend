import React, { useEffect, useState } from "react";
import { Button, Form, Input, Modal, Upload, message } from "antd";
import styled from "styled-components";
import { UploadOutlined } from "@ant-design/icons";
import Cookies from "universal-cookie";

function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  return isJpgOrPng;
}

const validateMessages = {
  // eslint-disable-next-line no-template-curly-in-string
  required: "${label} is required!",
  types: {
    // eslint-disable-next-line no-template-curly-in-string
    email: "Invalid email address!",
    // eslint-disable-next-line no-template-curly-in-string
    number: "${label} is not a valid number!",
  },
  number: {
    // eslint-disable-next-line no-template-curly-in-string
    range: "${label} must be between ${min} and ${max}",
  },
};

const UserSettings = ({
  editProfile,
  visible,
  setVisible,
  onCancel,
  userData,
}) => {
  const [form] = Form.useForm();
  const [userId, setUserId] = useState(null);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      // eslint-disable-next-line radix
      setUserId(parseInt(cookies.get("user_id")));
    }
  }, []);

  const onFinish = async (values) => {
    // eslint-disable-next-line no-debugger
    debugger;
    editProfile(userId, values);
    setVisible(false);
  };

  return (
    <Modal
      visible={visible}
      centered
      title="Settings"
      okText="Save settings"
      onCancel={onCancel}
      onOk={form.submit}
      width={670}
    >
      <StyledForm
        layout="vertical"
        name="user-settings"
        onFinish={onFinish}
        validateMessages={validateMessages}
        labelAlign="left"
        afterCancel={form.resetFields()}
        form={form}
      >
        <StyledFormItem name="login" label="Login">
          <StyledInput defaultValue={userData.login} disabled />
        </StyledFormItem>
        <StyledFormItem
          name="email"
          label="Email"
          rules={[
            {
              type: "email",
            },
            {
              required: true,
            },
          ]}
        >
          <StyledInput />
        </StyledFormItem>
        <StyledFormItem
          name="password"
          label="Password"
          rules={[
            {
              required: true,
            },
            {
              min: 5,
              message: "The password must be more than 4 characters long",
            },
          ]}
        >
          <StyledInputPassword />
        </StyledFormItem>
        <StyledFormItem name="firstName" label="First name" initialValue="">
          <StyledInput />
        </StyledFormItem>
        <StyledFormItem name="lastName" label="Last name" initialValue="">
          <StyledInput />
        </StyledFormItem>
        <StyledFormItem name="company" label="Company" initialValue="">
          <StyledInput />
        </StyledFormItem>
        <Form.Item name="upload" label="Avatar">
          <Upload
            method="GET"
            name="avatar"
            beforeUpload={beforeUpload}
            maxCount={1}
          >
            <Button icon={<UploadOutlined />}>Upload photo</Button>
          </Upload>
        </Form.Item>
      </StyledForm>
    </Modal>
  );
};

const StyledForm = styled(Form)`
  font-style: normal;
  font-family: "Noto Sans", serif;
  .ant-form-item-required {
    font-size: 16px;
    color: #787b86;
  }
  .ant-form-item-label label {
    font-size: 16px;
    color: #787b86;
  }
`;

const StyledFormItem = styled(Form.Item)`
  padding-bottom: 10px;
`;

const StyledInputPassword = styled(Input.Password)`
  height: 30px;
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 18px;
  font-style: normal;
`;

const StyledInput = styled(Input)`
  height: 30px;
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 14px;
  font-style: normal;
  font-family: "Noto Sans", serif;
`;

export default UserSettings;
