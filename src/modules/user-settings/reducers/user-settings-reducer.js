import * as actionTypes from "../types/types";

const initialState = {
  profile: null,
};

const userSettingsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.EDIT_PROFILE_SUCCESS:
      return {
        ...state,
        profile: action.profile,
      };
    default:
      return state;
  }
};

export default userSettingsReducer;
