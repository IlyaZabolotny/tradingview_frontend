import * as actionTypes from "../types/types";

const initialState = {
  ideas: [],
  pageNumber: 1,
  total: 0,
};

const ideasListReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_IDEAS_SUCCESS:
      return {
        ...state,
        ideas: action.ideas,
        pageNumber: action.pageNumber,
        total: action.total,
      };
    case actionTypes.FETCH_IDEAS_UNAUTHORIZED_SUCCESS:
      return {
        ...state,
        ideas: action.ideas,
        pageNumber: action.pageNumber,
        total: action.total,
      };
    case actionTypes.SET_LIKE_SUCCESS:
      return {
        ...state,
      };
    case actionTypes.REMOVE_LIKE_SUCCESS:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default ideasListReducer;
