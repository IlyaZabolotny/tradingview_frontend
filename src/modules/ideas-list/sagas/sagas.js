import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import {
  loadIdeas,
  loadIdeasUnauthorized,
  removeFavoriteIdeaSuccess,
  removeLikeSuccess,
  setFavoriteIdeaSuccess,
  setLikeSuccess,
} from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ userId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.keyword = keyword;
    result.pageNumber = pageNumber - 1;
    result.size = 3;
    const payload = yield call(service.getAllIdeas, result);
    yield put(
      loadIdeas(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* sagaUnauthorizedWorker({ pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.keyword = keyword;
    result.pageNumber = pageNumber - 1;
    result.size = 3;
    const payload = yield call(service.getAllIdeasUnauthorized, {
      params: result,
    });
    yield put(
      loadIdeasUnauthorized(
        payload.data.content,
        payload.data.number,
        payload.data.totalElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* likesWorker({ userId, ideaId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.ideaId = ideaId;
    yield call(service.setLike, result);
    yield put(setLikeSuccess());
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 3;
    const updatePayload = yield call(service.getAllIdeas, updateResult);
    yield put(
      loadIdeas(
        updatePayload.data.pageList,
        updatePayload.data.page,
        updatePayload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* removeLikeWorker({ userId, ideaId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.ideaId = ideaId;
    yield call(service.removeLike, result);
    yield put(removeLikeSuccess());
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 3;
    const updatePayload = yield call(service.getAllIdeas, updateResult);
    yield put(
      loadIdeas(
        updatePayload.data.pageList,
        updatePayload.data.page,
        updatePayload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* favoriteIdeaWorker({ userId, ideaId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.ideaId = ideaId;
    yield call(service.setFavoriteIdea, result);
    yield put(setFavoriteIdeaSuccess());
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 3;
    const updatePayload = yield call(service.getAllIdeas, updateResult);
    yield put(
      loadIdeas(
        updatePayload.data.pageList,
        updatePayload.data.page,
        updatePayload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* removeFavoriteIdeaWorker({ userId, ideaId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.ideaId = ideaId;
    yield call(service.removeFavoriteIdea, result);
    yield put(removeFavoriteIdeaSuccess());
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 3;
    const updatePayload = yield call(service.getAllIdeas, updateResult);
    yield put(
      loadIdeas(
        updatePayload.data.pageList,
        updatePayload.data.page,
        updatePayload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

export function* ideasWatcher() {
  yield takeLatest(actionTypes.FETCH_IDEAS_REQUEST, sagaWorker);
}

export function* ideasUnauthorizedWatcher() {
  yield takeLatest(
    actionTypes.FETCH_IDEAS_UNAUTHORIZED_REQUEST,
    sagaUnauthorizedWorker
  );
}

export function* ideasLikesWatcher() {
  yield takeLatest(actionTypes.SET_LIKE_REQUEST, likesWorker);
}

export function* ideasRemoveLikeWatcher() {
  yield takeLatest(actionTypes.REMOVE_LIKE_REQUEST, removeLikeWorker);
}

export function* addToFavoriteIdeasWatcher() {
  yield takeLatest(actionTypes.SET_FAVORITE_IDEA_REQUEST, favoriteIdeaWorker);
}

export function* removeFromFavoriteIdeasWatcher() {
  yield takeLatest(
    actionTypes.REMOVE_FAVORITE_IDEA_REQUEST,
    removeFavoriteIdeaWorker
  );
}
