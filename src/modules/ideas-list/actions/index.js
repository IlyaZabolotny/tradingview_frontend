import * as types from "../types/types";

export const requestIdeas = (userId, pageNumber, keyword) => {
  return {
    type: types.FETCH_IDEAS_REQUEST,
    userId,
    pageNumber,
    keyword,
  };
};

export const loadIdeas = (ideas, pageNumber, total, keyword) => {
  return {
    type: types.FETCH_IDEAS_SUCCESS,
    ideas,
    pageNumber,
    total,
    keyword,
  };
};

export const requestIdeasUnauthorized = (pageNumber, keyword) => {
  return {
    type: types.FETCH_IDEAS_UNAUTHORIZED_REQUEST,
    pageNumber,
    keyword,
  };
};

export const loadIdeasUnauthorized = (ideas, pageNumber, total, keyword) => {
  return {
    type: types.FETCH_IDEAS_UNAUTHORIZED_SUCCESS,
    ideas,
    pageNumber,
    total,
    keyword,
  };
};

export const setLikeRequest = (userId, ideaId, pageNumber, keyword) => {
  return {
    type: types.SET_LIKE_REQUEST,
    userId,
    ideaId,
    pageNumber,
    keyword,
  };
};

export const setLikeSuccess = () => {
  return {
    type: types.SET_LIKE_SUCCESS,
  };
};

export const removeLikeRequest = (userId, ideaId, pageNumber, keyword) => {
  return {
    type: types.REMOVE_LIKE_REQUEST,
    userId,
    ideaId,
    pageNumber,
    keyword,
  };
};

export const removeLikeSuccess = () => {
  return {
    type: types.REMOVE_LIKE_SUCCESS,
  };
};
export const setFavoriteIdeaRequest = (userId, ideaId, pageNumber, keyword) => {
  return {
    type: types.SET_FAVORITE_IDEA_REQUEST,
    userId,
    ideaId,
    pageNumber,
    keyword,
  };
};

export const setFavoriteIdeaSuccess = () => {
  return {
    type: types.SET_FAVORITE_IDEA_SUCCESS,
  };
};

export const removeFavoriteIdeaRequest = (
  userId,
  ideaId,
  pageNumber,
  keyword
) => {
  return {
    type: types.REMOVE_FAVORITE_IDEA_REQUEST,
    userId,
    ideaId,
    pageNumber,
    keyword,
  };
};

export const removeFavoriteIdeaSuccess = () => {
  return {
    type: types.REMOVE_FAVORITE_IDEA_SUCCESS,
  };
};
