import { connect } from "react-redux";
import {
  removeFavoriteIdeaRequest,
  removeLikeRequest,
  requestIdeas,
  requestIdeasUnauthorized,
  setFavoriteIdeaRequest,
  setLikeRequest,
} from "../actions";
import IdeasList from "../components/ideas-list";

const mapStateToProps = (state) => {
  return {
    ideas: state.ideas.ideas,
    total: state.ideas.total,
    pageNumber: state.ideas.pageNumber,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: (userId, pageNumber, keyword) => {
      dispatch(requestIdeas(userId, pageNumber, keyword));
    },
    fetchDataUnauthorized: (pageNumber, keyword) => {
      dispatch(requestIdeasUnauthorized(pageNumber, keyword));
    },
    setLike: (userId, ideaId, pageNumber, keyword) => {
      dispatch(setLikeRequest(userId, ideaId, pageNumber, keyword));
    },
    removeLike: (userId, ideaId, pageNumber, keyword) => {
      dispatch(removeLikeRequest(userId, ideaId, pageNumber, keyword));
    },
    setFavoriteIdea: (userId, ideaId, pageNumber, keyword) => {
      dispatch(setFavoriteIdeaRequest(userId, ideaId, pageNumber, keyword));
    },
    removeFavoriteIdea: (userId, ideaId, pageNumber, keyword) => {
      dispatch(removeFavoriteIdeaRequest(userId, ideaId, pageNumber, keyword));
    },
  };
};

const IdeasListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(IdeasList);

export default IdeasListContainer;
