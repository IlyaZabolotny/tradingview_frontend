import React, { useEffect, useState } from "react";
import { Input } from "antd";
import Cookies from "universal-cookie";
import styled from "styled-components";
import ContentList from "../../../components/content-list";

const IdeasList = ({
  fetchData,
  fetchDataUnauthorized,
  setLike,
  removeLike,
  setFavoriteIdea,
  removeFavoriteIdea,
  ideas,
  total,
}) => {
  const [page, setPage] = useState(1);
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      // eslint-disable-next-line no-debugger
      setAuthorized(true);
      // eslint-disable-next-line radix
      setUserId(parseInt(cookies.get("user_id")));
    }
  }, []);

  useEffect(() => {
    if (authorized) {
      fetchData(userId, 1, "");
    } else {
      fetchDataUnauthorized(1, "");
    }
  }, [authorized]);

  return (
    <IdeasWrapper>
      <StyledSearch
        id="search-ideas"
        placeholder="Search ideas"
        allowClear
        onSearch={(value) => {
          if (authorized) {
            fetchData(userId, 1, value);
          } else {
            fetchDataUnauthorized(1, value);
          }
          setPage(1);
        }}
      />
      <ContentList
        fetchData={fetchData}
        setLike={setLike}
        removeLike={removeLike}
        setFavoriteIdea={setFavoriteIdea}
        removeFavoriteIdea={removeFavoriteIdea}
        authorized={authorized}
        userId={userId}
        data={ideas}
        pageSize={3}
        pageNumber={page}
        total={total}
        onChange={(pageNumber) => {
          const searchValue = document.getElementById("search-ideas").value;
          if (authorized) {
            fetchData(userId, pageNumber, searchValue);
          } else {
            fetchDataUnauthorized(pageNumber, searchValue);
          }
          setPage(pageNumber);
        }}
      />
    </IdeasWrapper>
  );
};

const { Search } = Input;

const IdeasWrapper = styled.div`
  flex-basis: 80%;
  padding-right: 10px;
`;

const StyledSearch = styled(Search)`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-size: 18px;
  width: 50%;
  margin-bottom: 26px;
`;

export default IdeasList;
