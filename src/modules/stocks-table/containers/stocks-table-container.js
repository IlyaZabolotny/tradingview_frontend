import { connect } from "react-redux";
import { requestData } from "../actions";
import StocksTable from "../components/stocks-table";

const mapStateToProps = (state) => {
  const { stocks } = state;
  return {
    data: stocks.data,
    pageNumber: stocks.pageNumber,
    total: stocks.total,
    loading: stocks.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: (pageNumber, keyword) => {
      dispatch(requestData(pageNumber, keyword));
    },
  };
};

const StocksTableContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(StocksTable);

export default StocksTableContainer;
