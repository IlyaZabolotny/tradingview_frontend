import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { loadData } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ pageNumber, keyword }) {
  const service = new Service();
  try {
    // eslint-disable-next-line no-debugger
    debugger;
    const result = {};
    result.pageNumber = pageNumber - 1;
    result.keyword = keyword;
    result.size = 10;
    const payload = yield call(service.getStocks, { params: result });
    yield put(
      loadData(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

export function* stocksWatcher() {
  yield takeLatest(actionTypes.FETCH_DATA_REQUEST, sagaWorker);
}
