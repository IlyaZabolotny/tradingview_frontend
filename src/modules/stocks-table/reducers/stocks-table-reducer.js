import * as actionTypes from "../types/types";

const initialState = {
  data: [],
  pageNumber: 0,
  total: 0,
  loading: true,
};

const stocksTableReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_DATA_SUCCESS:
      return {
        data: action.data,
        pageNumber: action.pageNumber,
        total: action.total,
        loading: false,
      };
    default:
      return state;
  }
};

export default stocksTableReducer;
