import * as types from "../types/types";

export const requestData = (pageNumber, keyword) => {
  return {
    type: types.FETCH_DATA_REQUEST,
    pageNumber,
    keyword,
  };
};

export const loadData = (data, pageNumber, total, keyword) => {
  return {
    type: types.FETCH_DATA_SUCCESS,
    data,
    pageNumber,
    total,
    keyword,
  };
};
