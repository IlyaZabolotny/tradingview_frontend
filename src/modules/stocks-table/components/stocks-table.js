import React, { useEffect, useState } from "react";
import StockTab from "../../../components/tabs/stock-tab";

const StocksTable = ({ fetchData, data, total, loading }) => {
  const [page, setPage] = useState(1);

  useEffect(() => {
    fetchData(1, "");
  }, []);

  return (
    <StockTab
      data={data}
      pageSize={10}
      pageNumber={page}
      total={total}
      loading={loading}
      onChange={(pageNumber) => {
        const searchValue = document.getElementById("search-stocks").value;
        fetchData(pageNumber, searchValue);
        setPage(pageNumber);
      }}
      onSearch={(value) => {
        fetchData(1, value);
        setPage(1);
      }}
    />
  );
};

export default StocksTable;
