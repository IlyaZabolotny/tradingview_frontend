import { connect } from "react-redux";
import { fetchIdea } from "../actions";
import IdeaWatcherForm from "../components/idea-watcher-form";

const mapStateToProps = (state) => {
  return {
    title: state.ideaWatcher.title,
    content: state.ideaWatcher.content,
    image: state.ideaWatcher.image,
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchIdea: (ideaId) => {
    dispatch(fetchIdea(ideaId));
  },
});

const IdeaWatcherFormContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(IdeaWatcherForm);

export default IdeaWatcherFormContainer;
