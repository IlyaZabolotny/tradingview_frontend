import React, { useMemo } from "react";
import { Form, Input, Modal, Image, Button } from "antd";
import styled from "styled-components";

const IdeaWatcherForm = ({ data, visible, setVisible }) => {
  const [form] = Form.useForm();

  const object = useMemo(() => {
    // eslint-disable-next-line no-debugger
    debugger;
    if (data) {
      const { title, content, image } = data;
      return { title, content, image };
    }
    return {};
  }, [data]);

  return (
    <Modal
      visible={visible}
      centered
      title="Idea"
      form={form}
      onCancel={() => setVisible(false)}
      bodyStyle={{ alignContent: "center" }}
      footer={[
        <Button key="back" onClick={() => setVisible(false)}>
          Cancel
        </Button>,
      ]}
    >
      <StyledInput disabled value={object.title} />
      <StyledInputTextArea disabled rows={15} value={object.content} />
      <Image
        preview={false}
        width={475}
        height={250}
        src={`data:image/jpeg;base64,${object.image}`}
      />
    </Modal>
  );
};

const StyledInput = styled(Input)`
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 14px;
  font-style: normal;
  font-family: "Noto Sans", serif;
  margin-bottom: 20px;
`;

const StyledInputTextArea = styled(Input.TextArea)`
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 14px;
  font-style: normal;
  font-family: "Noto Sans", serif;
  margin-bottom: 20px;
`;

// const StyledForm = styled(Form)`
//   font-style: normal;
//   font-family: "Noto Sans", serif;
//   .ant-form-item-required {
//     font-style: normal;
//     font-family: "Noto Sans", serif;
//     font-size: 16px;
//   }
// `;

export default IdeaWatcherForm;
