import * as types from "../types/types";

export const fetchIdea = (ideaId) => {
  return {
    type: types.FETCH_IDEA_REQUEST,
    ideaId,
  };
};

export const ideaFetched = (title, content, image) => {
  return {
    type: types.FETCH_IDEA_SUCCESS,
    title,
    content,
    image,
  };
};
