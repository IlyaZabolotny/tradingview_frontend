import * as actionTypes from "../types/types";

const initialState = {
  title: "",
  content: "",
  image: null,
};

const ideaWatcherFormReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_IDEA_SUCCESS:
      return {
        ...state,
        title: action.title,
        content: action.content,
        image: action.image,
      };
    default:
      return state;
  }
};

export default ideaWatcherFormReducer;
