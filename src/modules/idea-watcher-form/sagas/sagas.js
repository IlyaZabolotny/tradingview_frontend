import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { ideaFetched } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ ideaId }) {
  const service = new Service();
  try {
    const idea = yield call(service.getIdea, ideaId);
    yield put(ideaFetched(idea.data.title, idea.data.content, idea.data.image));
  } catch (error) {
    console.error(error);
  }
}

export function* fetchIdeaWatcherById() {
  yield takeLatest(actionTypes.FETCH_IDEA_REQUEST, sagaWorker);
}
