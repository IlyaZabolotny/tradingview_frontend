import * as actionTypes from "../types/types";

const initialState = {
  idea: null,
};

const ideaFormReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CREATE_IDEA_SUCCESS:
      return {
        ...state,
        idea: action.idea,
      };
    default:
      return state;
  }
};

export default ideaFormReducer;
