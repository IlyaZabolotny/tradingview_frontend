import React, { useEffect, useState } from "react";
import {
  Button,
  Form,
  Input,
  Modal,
  Upload,
  message,
  notification,
} from "antd";
import styled from "styled-components";
import { UploadOutlined } from "@ant-design/icons";
import Cookies from "universal-cookie";

function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  return isJpgOrPng;
}

const validateMessages = {
  // eslint-disable-next-line no-template-curly-in-string
  required: "${label} is required!",
  types: {
    // eslint-disable-next-line no-template-curly-in-string
    email: "${label} is not a valid email!",
    // eslint-disable-next-line no-template-curly-in-string
    number: "${label} is not a valid number!",
  },
  number: {
    // eslint-disable-next-line no-template-curly-in-string
    range: "${label} must be between ${min} and ${max}",
  },
};

const IdeaForm = ({ createIdea, visible, onCancel, fetchData, setVisible }) => {
  const [form] = Form.useForm();
  const [userId, setUserId] = useState(null);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      // eslint-disable-next-line radix
      setUserId(parseInt(cookies.get("user_id")));
    }
  }, []);

  const openNotification = () => {
    notification.open({
      message: "You have published a new idea",
    });
  };

  return (
    <Modal
      style={{ width: 800 }}
      visible={visible}
      centered
      title="New idea"
      okText="Publish"
      form={form}
      onCancel={() => setVisible(false)}
      onOk={form.submit}
    >
      <StyledForm
        form={form}
        id="idea-form"
        layout="vertical"
        name="idea-form"
        validateMessages={validateMessages}
        onFinish={(values) => {
          createIdea(values, userId, onCancel, fetchData);
          openNotification();
        }}
        afterClose={form.resetFields()}
      >
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: "Please enter the idea title",
            },
          ]}
        >
          <StyledInput />
        </Form.Item>
        <Form.Item
          name="content"
          label="Content"
          rules={[
            {
              required: true,
              message: "Please enter the idea content",
            },
          ]}
        >
          <StyledInputTextArea rows={15} />
        </Form.Item>
        <Form.Item
          name="upload"
          rules={[
            {
              required: true,
              message: "Please upload the idea image",
            },
          ]}
        >
          <Upload
            method="GET"
            name="avatar"
            beforeUpload={beforeUpload}
            maxCount={1}
          >
            <ButtonUpload icon={<UploadOutlined />}>Upload image</ButtonUpload>
          </Upload>
        </Form.Item>
      </StyledForm>
    </Modal>
  );
};

const StyledInput = styled(Input)`
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 14px;
  font-style: normal;
  font-family: "Noto Sans", serif;
`;

const StyledInputTextArea = styled(Input.TextArea)`
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 14px;
  font-style: normal;
  font-family: "Noto Sans", serif;
`;

const StyledForm = styled(Form)`
  font-style: normal;
  font-family: "Noto Sans", serif;
  .ant-form-item-required {
    font-style: normal;
    font-family: "Noto Sans", serif;
    font-size: 16px;
  }
`;

const ButtonUpload = styled(Button)`
  margin-top: 10px;
`;

export default IdeaForm;
