import * as types from "../types/types";

export const createIdea = (values, userId, successAction, updateList) => {
  return {
    type: types.CREATE_IDEA_REQUEST,
    values,
    userId,
    successAction,
    updateList,
  };
};

export const ideaCreated = (idea) => {
  return {
    type: types.CREATE_IDEA_SUCCESS,
    idea,
  };
};
