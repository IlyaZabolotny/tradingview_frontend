import { connect } from "react-redux";
import { createIdea } from "../actions";
import IdeaForm from "../components/idea-form";
import { requestIdeas } from "../../user-ideas-list/actions";

const mapDispatchToProps = (dispatch) => ({
  createIdea: (values, userId, successAction, updateList) => {
    dispatch(createIdea(values, userId, successAction, updateList));
  },
  fetchData: (userId, pageNumber, keyword) => {
    dispatch(requestIdeas(userId, pageNumber, keyword));
  },
});

const IdeaFormContainer = connect(null, mapDispatchToProps)(IdeaForm);

export default IdeaFormContainer;
