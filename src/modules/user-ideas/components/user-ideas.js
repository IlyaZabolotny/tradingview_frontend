import React, { useEffect, useState } from "react";
import { Button } from "antd";
import styled from "styled-components";
import Cookies from "universal-cookie";
import FavoriteIdeasListContainer from "../../favorite-ideas-list/containers/favorite-ideas-list-container";
import UserIdeasListContainer from "../../user-ideas-list/containers/user-ideas-list-container";

const UserIdeas = ({ anotherUserId }) => {
  const [toggleState, setToggleState] = useState(1);
  const [anotherUser, setAnotherUser] = useState(false);
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      // eslint-disable-next-line no-debugger
      setAuthorized(true);
      // eslint-disable-next-line radix
      setUserId(cookies.get("user_id"));
    }
  }, []);

  useEffect(() => {
    // eslint-disable-next-line no-debugger
    debugger;
    if (userId) {
      if (userId !== anotherUserId) {
        setAnotherUser(true);
      }
    }
  }, [authorized]);

  const ButtonClick = (index) => {
    setToggleState(index);
  };

  return toggleState === 1 ? (
    <Wrapper>
      <ButtonWrapper>
        <ChoiceButtonFocused type="dashed" onClick={() => ButtonClick(1)}>
          Published
        </ChoiceButtonFocused>
        {!anotherUser && (
          <ChoiceButton type="dashed" onClick={() => ButtonClick(2)}>
            Favorite ideas
          </ChoiceButton>
        )}
      </ButtonWrapper>
      <UserIdeasListContainer anotherUserId={anotherUserId} />
    </Wrapper>
  ) : (
    <Wrapper>
      <ButtonWrapper>
        <ChoiceButton type="dashed" onClick={() => ButtonClick(1)}>
          Published
        </ChoiceButton>
        <ChoiceButton type="dashed" onClick={() => ButtonClick(2)}>
          Favorite ideas
        </ChoiceButton>
      </ButtonWrapper>
      <FavoriteIdeasListContainer />
    </Wrapper>
  );
  // return (
  //   <Wrapper>
  //     <ButtonWrapper>
  //       <ChoiceButton type="dashed" onClick={() => ButtonClick(1)}>
  //         Published
  //       </ChoiceButton>
  //       <ChoiceButton type="dashed" onClick={() => ButtonClick(2)}>
  //         Favorite ideas
  //       </ChoiceButton>
  //     </ButtonWrapper>
  //     <FavoriteIdeasListContainer />
  //   </Wrapper>
  // );
};

const Wrapper = styled.div`
  padding-top: 20px;
`;

const ButtonWrapper = styled.div`
  margin-bottom: 20px;
`;

const ChoiceButton = styled(Button)`
  height: 35px;
  width: 200px;
  margin: 0 5px;
  font-style: normal;
  font-size: 14px;
  text-align: center;
  border-radius: 5px;
  font-family: "Noto Sans", serif;
`;

const ChoiceButtonFocused = styled(Button)`
  height: 35px;
  width: 200px;
  margin: 0 0px;
  font-style: normal;
  font-size: 14px;
  text-align: center;
  border-radius: 5px;
  font-family: "Noto Sans", serif;
  color: #40a9ff;
  background: #fff;
  border-color: #40a9ff;
`;
export default UserIdeas;
