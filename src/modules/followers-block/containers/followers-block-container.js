import { connect } from "react-redux";
import { addFollowingRequest, deleteFollowingRequest } from "../actions";
import FollowersBlock from "../components/followers-block";

const mapDispatchToProps = (dispatch) => {
  return {
    addFollowing: (userId, subscribedTo, pageNumber, keyword) => {
      dispatch(addFollowingRequest(userId, subscribedTo, pageNumber, keyword));
    },
    deleteFollowing: (userId, subscribedTo, pageNumber, keyword) => {
      dispatch(
        deleteFollowingRequest(userId, subscribedTo, pageNumber, keyword)
      );
    },
  };
};

const FollowersBlockContainer = connect(
  null,
  mapDispatchToProps
)(FollowersBlock);

export default FollowersBlockContainer;
