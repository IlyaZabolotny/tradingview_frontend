import * as types from "../types/types";

export const addFollowingSuccess = () => {
  return {
    type: types.ADD_FOLLOWING_SUCCESS,
  };
};

export const addFollowingRequest = (
  userId,
  subscribedTo,
  pageNumber,
  keyword
) => {
  return {
    type: types.ADD_FOLLOWING_REQUEST,
    userId,
    subscribedTo,
    pageNumber,
    keyword,
  };
};

export const deleteFollowingSuccess = () => {
  return {
    type: types.DELETE_FOLLOWING_FOLLOWERS_SUCCESS,
  };
};

export const deleteFollowingRequest = (
  userId,
  subscribedTo,
  pageNumber,
  keyword
) => {
  return {
    type: types.DELETE_FOLLOWING_FOLLOWERS_REQUEST,
    userId,
    subscribedTo,
    pageNumber,
    keyword,
  };
};
