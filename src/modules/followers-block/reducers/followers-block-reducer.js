import * as actionTypes from "../types/types";

const initialState = {};

const followersBlockReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.ADD_FOLLOWING_SUCCESS:
      return {
        ...state,
      };
    case actionTypes.DELETE_FOLLOWING_FOLLOWERS_SUCCESS:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default followersBlockReducer;
