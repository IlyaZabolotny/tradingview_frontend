import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { addFollowingSuccess, deleteFollowingSuccess } from "../actions";
import * as actionTypes from "../types/types";
import { loadFollowers } from "../../followers-list/actions";
import { loadFollowings } from "../../followings-list/actions";

function* addFollowingWorker({ userId, subscribedTo, pageNumber, keyword }) {
  const service = new Service();
  try {
    // eslint-disable-next-line no-debugger
    debugger;
    const result = {};
    result.userId = userId;
    result.subscribedTo = subscribedTo;
    yield call(service.follow, result);
    yield put(addFollowingSuccess());
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 5;
    const payload = yield call(service.getFollowers, updateResult);
    yield put(
      loadFollowers(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        keyword
      )
    );
    const resultFollowings = {};
    resultFollowings.userId = userId;
    resultFollowings.keyword = keyword;
    resultFollowings.pageNumber = pageNumber - 1;
    resultFollowings.size = 5;
    const payloadFollowings = yield call(
      service.getFollowings,
      resultFollowings
    );
    yield put(
      loadFollowings(
        payloadFollowings.data.pageList,
        payloadFollowings.data.page,
        payloadFollowings.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* deleteFollowingWorker({ userId, subscribedTo, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.subscribedTo = subscribedTo;
    yield call(service.unfollow, result);
    yield put(deleteFollowingSuccess());
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 5;
    const payload = yield call(service.getFollowers, updateResult);
    yield put(
      loadFollowers(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        keyword
      )
    );
    const resultFollowings = {};
    resultFollowings.userId = userId;
    resultFollowings.keyword = keyword;
    resultFollowings.pageNumber = pageNumber - 1;
    resultFollowings.size = 5;
    const payloadFollowings = yield call(
      service.getFollowings,
      resultFollowings
    );
    yield put(
      loadFollowings(
        payloadFollowings.data.pageList,
        payloadFollowings.data.page,
        payloadFollowings.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

export function* addFollowingWatcher() {
  yield takeLatest(actionTypes.ADD_FOLLOWING_REQUEST, addFollowingWorker);
}

export function* deleteFollowingFollowersWatcher() {
  yield takeLatest(
    actionTypes.DELETE_FOLLOWING_FOLLOWERS_REQUEST,
    deleteFollowingWorker
  );
}
