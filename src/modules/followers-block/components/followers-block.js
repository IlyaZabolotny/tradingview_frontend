import React from "react";
import { Button } from "antd";
import { StockOutlined, UserOutlined } from "@ant-design/icons";
import styled from "styled-components";

const FollowersBlock = ({
  users,
  ideas,
  subscribed,
  addFollowing,
  deleteFollowing,
  pageNumber,
  userId,
  subscribedTo,
  anotherUser,
}) => {
  return (
    <Wrapper>
      <ItemWrapper>
        <UserOutlined style={{ color: "#C8C8C8" }} />
        <Text>{users}</Text>
      </ItemWrapper>
      <ItemWrapper>
        <StockOutlined style={{ color: "#C8C8C8" }} />
        <Text>{ideas}</Text>
      </ItemWrapper>
      {!anotherUser && (
        <ItemWrapper>
          {!subscribed ? (
            <StyledAddButton
              type="primary"
              onClick={() => {
                // eslint-disable-next-line no-debugger
                debugger;
                const searchValue =
                  document.getElementById("followers-list").value;
                addFollowing(userId, subscribedTo, pageNumber, searchValue);
              }}
            >
              Follow
            </StyledAddButton>
          ) : (
            <StyledButton
              type="primary"
              onClick={() => {
                // eslint-disable-next-line no-debugger
                debugger;
                const searchValue =
                  document.getElementById("followers-list").value;
                deleteFollowing(userId, subscribedTo, pageNumber, searchValue);
              }}
            >
              Unfollow
            </StyledButton>
          )}
        </ItemWrapper>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
`;

const ItemWrapper = styled.div`
  margin: 0 20px;
  padding: 10px;
  width: 100%;
  display: flex;
`;

const Text = styled.div`
  padding-left: 10px;
  font-size: 16px;
  color: #c8c8c8;
`;

const StyledButton = styled(Button)`
  width: 150px;
  height: 40px;
  border-radius: 5px;
`;

const StyledAddButton = styled(Button)`
  background: #3faa9e;
  border: 1px solid #3faa9e;
  box-sizing: border-box;
  border-radius: 5px;
  color: #ffff;
  font-family: "Noto Sans", serif;
  width: 150px;
  height: 40px;
  border-radius: 5px;
  :active {
    border: 1px solid #3faa9e;
    color: #3faa9e;
  }
  :focus {
    border: 1px solid #2196f3;
    color: #3faa9e;
  }
  :hover {
    border: 1px solid #3faa9e;
    color: white;
    background: #3faa9e;
  }
`;

export default FollowersBlock;
