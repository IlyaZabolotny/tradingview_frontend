import * as actionTypes from "../types/types";

const initialState = {
  searchValues: [],
};

const mainTradingViewHeaderReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_SEARCH_VALUES_SUCCESS:
      return {
        ...state,
        searchValues: action.searchValues,
      };
    default:
      return state;
  }
};

export default mainTradingViewHeaderReducer;
