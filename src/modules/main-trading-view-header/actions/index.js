import * as types from "../types/types";

export const loadData = (searchValues) => {
  return {
    type: types.FETCH_SEARCH_VALUES_SUCCESS,
    searchValues,
  };
};

export const requestData = (searchText) => {
  return {
    type: types.FETCH_SEARCH_VALUES_REQUEST,
    searchText,
  };
};
