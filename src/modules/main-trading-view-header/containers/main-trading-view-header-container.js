import { connect } from "react-redux";
import { requestData } from "../actions";
import MainTradingViewHeader from "../components/main-trading-view-header";

const mapStateToProps = (state) => {
  const { header } = state;
  return {
    searchValues: header.searchValues,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchSearchValues: (searchText) => {
      dispatch(requestData(searchText));
    },
  };
};

const MainTradingViewHeaderContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MainTradingViewHeader);

export default MainTradingViewHeaderContainer;
