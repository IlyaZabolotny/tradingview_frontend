import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { loadData } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ searchText }) {
  const service = new Service();
  try {
    const result = {};
    result.keyword = searchText;
    const payload = yield call(service.searchStock, { params: result });
    yield put(loadData(payload.data));
  } catch (error) {
    console.log(error);
  }
}

export function* tradingViewHeaderWatcher() {
  yield takeLatest(actionTypes.FETCH_SEARCH_VALUES_REQUEST, sagaWorker);
}
