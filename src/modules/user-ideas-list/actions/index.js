import * as types from "../types/types";

export const requestIdeas = (userId, pageNumber, keyword) => {
  return {
    type: types.FETCH_USER_IDEAS_REQUEST,
    userId,
    pageNumber,
    keyword,
  };
};

export const ideasLoaded = (userIdeas, pageNumber, total, keyword) => {
  return {
    type: types.FETCH_USER_IDEAS_SUCCESS,
    userIdeas,
    pageNumber,
    total,
    keyword,
  };
};

export const ideasError = (error) => {
  return {
    type: types.FETCH_USER_IDEAS_FAILURE,
    payload: error,
  };
};
