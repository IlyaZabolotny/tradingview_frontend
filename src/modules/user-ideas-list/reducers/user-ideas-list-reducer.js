import * as actionTypes from "../types/types";

const initialState = {
  userIdeas: [],
  pageNumber: 0,
  total: 0,
};

const userIdeasListReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_USER_IDEAS_SUCCESS:
      return {
        ...state,
        userIdeas: action.userIdeas,
        pageNumber: action.pageNumber,
        total: action.total,
      };
    default:
      return state;
  }
};

export default userIdeasListReducer;
