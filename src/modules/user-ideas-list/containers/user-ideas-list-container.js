import { connect } from "react-redux";
import { requestIdeas } from "../actions";
import UserIdeasList from "../components/user-ideas-list";

const mapStateToProps = (state) => {
  const { userIdeas } = state;
  return {
    userIdeas: userIdeas.userIdeas,
    total: userIdeas.total,
    pageNumber: userIdeas.pageNumber,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: (userId, pageNumber, keyword) => {
      dispatch(requestIdeas(userId, pageNumber, keyword));
    },
  };
};

const UserIdeasListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserIdeasList);

export default UserIdeasListContainer;
