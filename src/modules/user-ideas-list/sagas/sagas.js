import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import * as actionTypes from "../types/types";
import { ideasLoaded } from "../actions";

function* sagaWorker({ userId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.keyword = keyword;
    result.pageNumber = pageNumber - 1;
    result.size = 2;
    const payload = yield call(service.getAuthorIdeas, result);
    yield put(
      ideasLoaded(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}
export function* userIdeasWatcher() {
  yield takeLatest(actionTypes.FETCH_USER_IDEAS_REQUEST, sagaWorker);
}
