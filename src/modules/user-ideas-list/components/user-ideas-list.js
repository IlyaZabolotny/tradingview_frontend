import React, { useEffect, useState } from "react";
import { Avatar, Button, Image, Input, List } from "antd";
import { LikeOutlined } from "@ant-design/icons";
import styled from "styled-components";
import Cookies from "universal-cookie";
import moment from "moment";
import IdeaFormContainer from "../../idea-form/containers/idea-form-container";

const UserIdeasList = ({ fetchData, userIdeas, total, anotherUserId }) => {
  const [page, setPage] = useState(1);
  const [visible, setVisible] = useState(false);
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);
  const [anotherUser, setAnotherUser] = useState(false);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      // eslint-disable-next-line no-debugger
      setAuthorized(true);
      // eslint-disable-next-line radix
      setUserId(cookies.get("user_id"));
    }
  }, []);

  useEffect(() => {
    if (userId) {
      if (userId !== anotherUserId) {
        fetchData(anotherUserId, 1, "");
        setAnotherUser(true);
        setUserId(anotherUserId);
      } else {
        fetchData(userId, 1, "");
      }
    }
  }, [authorized]);

  return (
    <div>
      <StyledSearch
        id="search-user-ideas"
        placeholder="Search ideas"
        onSearch={(value) => {
          fetchData(userId, page, value);
          setPage(1);
        }}
      />
      {!anotherUser && (
        <>
          <StyledButton type="primary" onClick={() => setVisible(true)}>
            Create an idea
          </StyledButton>
          <IdeaFormContainer
            visible={visible}
            onCancel={() => setVisible(false)}
            setVisible={setVisible}
            fetchData={fetchData}
          />
        </>
      )}

      <StyledList
        itemLayout="vertical"
        pagination={{
          onChange: (pageNumber) => {
            const searchValue =
              document.getElementById("search-user-ideas").value;
            fetchData(userId, pageNumber, searchValue);
            setPage(pageNumber);
          },
          pageSize: 2,
          total,
          current: page,
        }}
        authorized={authorized}
        dataSource={userIdeas}
        renderItem={(item) => (
          <List.Item
            key={item.id}
            extra={
              <Image
                src={`data:image/jpeg;base64,${item.image}`}
                preview={false}
                width={300}
                height={150}
              />
            }
          >
            <StyledListItemMeta
              avatar={<Avatar src={`data:image/jpeg;base64,${item.avatar}`} />}
              title={item.login}
              description={moment(item.created).format("MMM Do")}
            />
            <ContentWrapper>
              <StyledTitle>{item.title}</StyledTitle>
              {item.content}
            </ContentWrapper>
            <ActionWrapper>
              <ItemWrapper>
                <StyledLikeOutlined />
                {item.likesCount}
              </ItemWrapper>
            </ActionWrapper>
          </List.Item>
        )}
      />
    </div>
  );
};

const { Search } = Input;

const StyledList = styled(List)`
  background: white;
  padding: 10px;
  font-size: 16px;
  font-family: "Noto Sans", serif;
  border: 1px solid #e0e3eb;
  border-radius: 5px;
`;

const StyledListItemMeta = styled(List.Item.Meta)`
  .ant-list-item-meta-title {
    font-size: 25px;
    font-family: "Noto Sans", serif;
  }
  .ant-list-item-meta-description {
    font-size: 14px;
    font-family: "Noto Sans", serif;
    color: #787b86;
  }
`;

const StyledSearch = styled(Search)`
  margin-bottom: 20px;
  width: 30%;
  font-family: "Noto Sans", serif;
  font-style: normal;
  .ant-input {
    height: 35px;
  }
  .ant-input-search-button {
    height: 35px;
  }
`;

const StyledTitle = styled.div`
  font-weight: bold;
`;

const ContentWrapper = styled.div`
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
`;

const ItemWrapper = styled.div`
  padding-right: 10px;
`;

const StyledLikeOutlined = styled(LikeOutlined)`
  margin-right: 5px;
`;

const ActionWrapper = styled.div`
  margin: 5px 0;
  display: flex;
`;

const StyledButton = styled(Button)`
  height: 35px;
  width: 300px;
  margin: 0 5px;
  font-style: normal;
  font-weight: 600;
  font-size: 18px;
  line-height: 17px;
  text-align: center;
  border-radius: 5px;
  font-family: "Noto Sans", serif;
`;

export default UserIdeasList;
