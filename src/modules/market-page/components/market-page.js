import React, { useEffect } from "react";
import { Row, Col, List } from "antd";
import styled from "styled-components";
import QuoteBlock from "../../../components/quote-block";

const MarketPage = ({ fetchMarketData, loading, actives, gainers, losers }) => {
  useEffect(() => {
    fetchMarketData();
  }, []);

  return (
    <div>
      <StyledHeader>MARKET QUOTES</StyledHeader>
      <StyledBorder />
      <ListWrapper>
        <Row gutter={50}>
          <Col span={8}>
            <StyledText>Actives</StyledText>
            <StyledList
              loading={loading}
              dataSource={actives}
              itemLayout="horizontal"
              renderItem={(item) => (
                <List.Item
                  key={`actives-${item.ticker}`}
                  extra={
                    <QuoteBlock
                      changes={item.changes}
                      changesPercentage={item.changesPercentage}
                      price={item.price}
                    />
                  }
                >
                  <List.Item.Meta
                    title={item.ticker}
                    description={item.companyName}
                  />
                </List.Item>
              )}
            />
          </Col>
          <Col span={8}>
            <StyledText>Gainers</StyledText>
            <StyledList
              loading={loading}
              dataSource={gainers}
              itemLayout="horizontal"
              renderItem={(item) => (
                <List.Item
                  key={`gainers-${item.ticker}`}
                  extra={
                    <QuoteBlock
                      changes={item.changes}
                      changesPercentage={item.changesPercentage}
                      price={item.price}
                    />
                  }
                >
                  <List.Item.Meta
                    title={item.ticker}
                    description={item.companyName}
                  />
                </List.Item>
              )}
            />
          </Col>
          <Col span={8}>
            <StyledText>Losers</StyledText>
            <StyledList
              loading={loading}
              dataSource={losers}
              itemLayout="horizontal"
              renderItem={(item) => (
                <List.Item
                  key={`losers-${item.ticker}`}
                  extra={
                    <QuoteBlock
                      changes={item.changes}
                      changesPercentage={item.changesPercentage}
                      price={item.price}
                    />
                  }
                >
                  <List.Item.Meta
                    title={item.ticker}
                    description={item.companyName}
                  />
                </List.Item>
              )}
            />
          </Col>
        </Row>
      </ListWrapper>
    </div>
  );
};

const StyledHeader = styled.div`
  font-family: "Noto Sans", serif;
  font-weight: 600;
  font-size: 30px;
  line-height: 43px;
  letter-spacing: 1px;
  margin: 24px 0;
  color: #131722;
`;

const StyledText = styled.div`
  font-style: normal;
  font-weight: 500;
  font-size: 30px;
  line-height: 37px;
  margin: 0 0 20px 5px;
`;

const StyledBorder = styled.div`
  border-bottom-width: 2px;
  border-bottom-style: solid;
  border-bottom-color: #e0e3eb;
`;

const ListWrapper = styled.div`
  margin: 43px 0;
`;

const StyledList = styled(List)`
  background: white;
  border: 1px solid #e0e3eb;
  border-radius: 5px;
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 500;
  line-height: 30px;
  overflow: auto;
  height: 590px;
  .ant-list-item-meta-title {
    font-size: 15px;
    font-weight: normal;
  }
  .ant-list-header {
    font-style: normal;
    font-weight: 500;
    font-size: 30px;
    line-height: 37px;
  }
  padding: 0 10px;
`;

export default MarketPage;
