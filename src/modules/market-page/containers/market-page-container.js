import { connect } from "react-redux";
import { requestData } from "../actions";
import MarketPage from "../components/market-page";

const mapStateToProps = (state) => {
  const { marketPage } = state;
  return {
    actives: marketPage.actives,
    gainers: marketPage.gainers,
    losers: marketPage.losers,
    loading: marketPage.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchMarketData: () => {
      dispatch(requestData());
    },
  };
};

const MarketPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(MarketPage);

export default MarketPageContainer;
