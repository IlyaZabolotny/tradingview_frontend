import * as types from "../types/types";

export const loadData = (actives, gainers, losers) => {
  return {
    type: types.FETCH_MARKET_DATA_SUCCESS,
    actives,
    gainers,
    losers,
  };
};

export const requestData = () => {
  return {
    type: types.FETCH_MARKET_DATA_REQUEST,
  };
};
