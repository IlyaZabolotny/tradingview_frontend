import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { loadData } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker() {
  const service = new Service();
  try {
    const actives = yield call(service.getMarketActives);
    const gainers = yield call(service.getMarketGainers);
    const losers = yield call(service.getMarketLosers);
    yield put(loadData(actives.data, gainers.data, losers.data));
  } catch (error) {
    console.log(error);
  }
}

export function* marketPageWatcher() {
  yield takeLatest(actionTypes.FETCH_MARKET_DATA_REQUEST, sagaWorker);
}
