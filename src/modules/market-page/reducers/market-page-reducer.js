import * as actionTypes from "../types/types";

const initialState = {
  actives: [],
  gainers: [],
  losers: [],
  loading: true,
};

const marketPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_MARKET_DATA_SUCCESS:
      return {
        ...state,
        actives: action.actives,
        gainers: action.gainers,
        losers: action.losers,
        loading: false,
      };
    default:
      return state;
  }
};

export default marketPageReducer;
