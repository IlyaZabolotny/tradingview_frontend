import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import {
  followUserSuccess,
  loadUsers,
  loadUsersAuthorized,
  unfollowUserSuccess,
} from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ pageNumber, pageSize, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.keyword = keyword;
    result.pageNumber = pageNumber - 1;
    result.size = pageSize;
    const payload = yield call(service.getUsersUnauthorized, {
      params: result,
    });
    yield put(
      loadUsers(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        pageSize,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* usersAuthWorker({ userId, pageNumber, pageSize, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.keyword = keyword;
    result.pageNumber = pageNumber - 1;
    result.size = pageSize;
    const payload = yield call(service.getUsers, result);
    yield put(
      loadUsersAuthorized(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        pageSize,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* followUserWorker({
  userId,
  subscribedTo,
  pageNumber,
  pageSize,
  keyword,
}) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.subscribedTo = subscribedTo;
    yield call(service.follow, result);
    yield put(followUserSuccess());
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = pageSize;
    const payload = yield call(service.getUsers, updateResult);
    yield put(
      loadUsersAuthorized(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        pageSize,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* unfollowUserWorker({
  userId,
  subscribedTo,
  pageNumber,
  pageSize,
  keyword,
}) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.subscribedTo = subscribedTo;
    yield call(service.unfollow, result);
    yield put(unfollowUserSuccess());
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = pageSize;
    const payload = yield call(service.getUsers, updateResult);
    yield put(
      loadUsersAuthorized(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        pageSize,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

export function* usersWatcher() {
  yield takeLatest(actionTypes.FETCH_USERS_REQUEST, sagaWorker);
}

export function* usersAuthWatcher() {
  yield takeLatest(actionTypes.FETCH_USERS_AUTHORIZED_REQUEST, usersAuthWorker);
}

export function* followUserWatcher() {
  yield takeLatest(actionTypes.FOLLOW_REQUEST, followUserWorker);
}

export function* unfollowUserWatcher() {
  yield takeLatest(actionTypes.UNFOLLOW_REQUEST, unfollowUserWorker);
}
