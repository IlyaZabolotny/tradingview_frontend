import React, { useEffect, useState } from "react";
import { Input, List, Avatar, Button } from "antd";
import styled from "styled-components";
import Cookies from "universal-cookie";
import { v4 as uuidv4 } from "uuid";
import { Link } from "react-router-dom";
import AuthorsBlock from "../../../components/authors-block";

const UsersList = ({
  getUsers,
  getUsersAuthorized,
  followUser,
  unfollowUser,
  users,
  pageSize,
  total,
}) => {
  const [page, setPage] = useState(1);
  // eslint-disable-next-line no-unused-vars
  const [userId, setUserId] = useState(null);
  // eslint-disable-next-line no-unused-vars
  const [authorized, setAuthorized] = useState(false);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      // eslint-disable-next-line radix
      setUserId(parseInt(cookies.get("user_id")));
    }
  }, []);

  useEffect(() => {
    if (authorized) {
      getUsersAuthorized(userId, 1, 7, "");
    } else {
      getUsers(1, 7, "");
    }
  }, [authorized]);

  return (
    <div>
      <AuthorsSearch
        id="user-search"
        placeholder="Search author"
        allowClear
        onSearch={(value) => {
          if (authorized) {
            getUsersAuthorized(userId, 1, 7, value);
          } else {
            getUsers(1, 7, value);
          }
          setPage(1);
        }}
      />
      <StyledList
        itemLayout="horizontal"
        pagination={{
          onChange: (pageNumber) => {
            const searchValue = document.getElementById("user-search").value;
            if (!authorized) {
              getUsers(pageNumber, 7, searchValue);
            } else {
              getUsersAuthorized(userId, pageNumber, 7, searchValue);
            }
            setPage(pageNumber);
          },
          pageSize,
          total,
          current: page,
        }}
        dataSource={users}
        renderItem={(item) => (
          <List.Item
            key={uuidv4()}
            extra={
              // eslint-disable-next-line no-nested-ternary
              authorized && !(item.id === userId) ? (
                !item.subscribed ? (
                  <StyledButton
                    onClick={() => {
                      const searchValue =
                        document.getElementById("user-search").value;
                      followUser(userId, item.id, page, 7, searchValue);
                    }}
                  >
                    Follow
                  </StyledButton>
                ) : (
                  <StyledRemoveButton
                    onClick={() => {
                      const searchValue =
                        document.getElementById("user-search").value;
                      unfollowUser(userId, item.id, page, 7, searchValue);
                    }}
                  >
                    Unfollow
                  </StyledRemoveButton>
                )
              ) : null
            }
          >
            <StyledListItemMeta
              avatar={
                <Avatar
                  shape="square"
                  src={`data:image/jpeg;base64,${item.avatar}`}
                />
              }
              title={<Link to={`user/${item.id}`}>{item.login}</Link>}
              description=<AuthorsBlock
                followers={item.followers}
                ideas={item.ideas}
              />
            />
          </List.Item>
        )}
      />
    </div>
  );
};

const { Search } = Input;

const AuthorsSearch = styled(Search)`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-size: 18px;
  width: 70%;
  margin-bottom: 10px;
`;

const StyledList = styled(List)`
  background: white;
  border: 1px solid #e0e3eb;
  border-radius: 5px;
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 500;
  line-height: 30px;
  height: 640px;
  .ant-list-item-meta-title {
    font-size: 16px;
  }
  padding: 0 10px;
`;

const StyledButton = styled(Button)`
  background: #ffffff;
  border: 1px solid #38a3f3;
  box-sizing: border-box;
  border-radius: 5px;
  color: #2196f3;
  font-family: "Noto Sans", serif;
  width: 200px;
  border-radius: 5px;
  :active {
    border: 1px solid #2196f3;
    color: #2196f3;
  }
  :focus {
    border: 1px solid #2196f3;
    color: #2196f3;
  }
  :hover {
    border: 1px solid #2196f3;
    color: white;
    background: #2196f3;
  }
`;

const StyledRemoveButton = styled(Button)`
  background: #ffffff;
  border: 1px solid #ef5350;
  box-sizing: border-box;
  border-radius: 5px;
  color: #ef5350;
  font-family: "Noto Sans", serif;
  width: 200px;
  border-radius: 5px;
  :active {
    border: 1px solid #ef5350;
    color: #ef5350;
  }
  :focus {
    border: 1px solid #ef5350;
    color: #ef5350;
  }
  :hover {
    border: 1px solid #ef5350;
    color: white;
    background: #ef5350;
  }
`;

const StyledListItemMeta = styled(List.Item.Meta)`
  .ant-avatar {
    margin-top: 12px;
  }
`;

export default UsersList;
