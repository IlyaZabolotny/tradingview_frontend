import * as actionTypes from "../types/types";

const initialState = {
  users: [],
  pageNumber: 1,
  total: 0,
  pageSize: 7,
};

const usersListReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_USERS_SUCCESS:
      return {
        ...state,
        users: action.users,
        pageNumber: action.pageNumber,
        total: action.total,
        pageSize: action.pageSize,
      };
    case actionTypes.FETCH_USERS_AUTHORIZED_SUCCESS:
      return {
        ...state,
        users: action.users,
        pageNumber: action.pageNumber,
        total: action.total,
        pageSize: action.pageSize,
      };
    case actionTypes.FOLLOW_SUCCESS:
      return {
        ...state,
      };
    case actionTypes.UNFOLLOW_SUCCESS:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default usersListReducer;
