import * as types from "../types/types";

export const loadUsers = (users, pageNumber, total, pageSize, keyword) => {
  return {
    type: types.FETCH_USERS_SUCCESS,
    users,
    pageNumber,
    total,
    pageSize,
    keyword,
  };
};

export const requestUsers = (pageNumber, pageSize, keyword) => {
  return {
    type: types.FETCH_USERS_REQUEST,
    pageNumber,
    pageSize,
    keyword,
  };
};

export const loadUsersAuthorized = (
  users,
  pageNumber,
  total,
  pageSize,
  keyword
) => {
  return {
    type: types.FETCH_USERS_AUTHORIZED_SUCCESS,
    users,
    pageNumber,
    total,
    pageSize,
    keyword,
  };
};

export const requestUsersAuthorized = (
  userId,
  pageNumber,
  pageSize,
  keyword
) => {
  return {
    type: types.FETCH_USERS_AUTHORIZED_REQUEST,
    userId,
    pageNumber,
    pageSize,
    keyword,
  };
};

export const followUserRequest = (
  userId,
  subscribedTo,
  pageNumber,
  pageSize,
  keyword
) => {
  return {
    type: types.FOLLOW_REQUEST,
    userId,
    subscribedTo,
    pageNumber,
    pageSize,
    keyword,
  };
};

export const unfollowUserSuccess = () => {
  return {
    type: types.UNFOLLOW_SUCCESS,
  };
};

export const unfollowUserRequest = (
  userId,
  subscribedTo,
  pageNumber,
  pageSize,
  keyword
) => {
  return {
    type: types.UNFOLLOW_REQUEST,
    userId,
    subscribedTo,
    pageNumber,
    pageSize,
    keyword,
  };
};

export const followUserSuccess = () => {
  return {
    type: types.FOLLOW_SUCCESS,
  };
};

export const usersError = (error) => {
  return {
    type: types.FETCH_USERS_FAILURE,
    payload: error,
  };
};
