import { connect } from "react-redux";
import {
  followUserRequest,
  requestUsers,
  requestUsersAuthorized,
  unfollowUserRequest,
} from "../actions";
import UsersList from "../components/users-list";

const mapStateToProps = (state) => {
  const { users } = state;
  return {
    users: users.users,
    pageNumber: users.pageNumber,
    total: users.total,
    pageSize: users.pageSize,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getUsers: (pageNumber, pageSize, keyword) => {
      dispatch(requestUsers(pageNumber, pageSize, keyword));
    },
    getUsersAuthorized: (userId, pageNumber, pageSize, keyword) => {
      dispatch(requestUsersAuthorized(userId, pageNumber, pageSize, keyword));
    },
    followUser: (userId, subscribedTo, pageNumber, pageSize, keyword) => {
      dispatch(
        followUserRequest(userId, subscribedTo, pageNumber, pageSize, keyword)
      );
    },
    unfollowUser: (userId, subscribedTo, pageNumber, pageSize, keyword) => {
      dispatch(
        unfollowUserRequest(userId, subscribedTo, pageNumber, pageSize, keyword)
      );
    },
  };
};

const UsersListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList);

export default UsersListContainer;
