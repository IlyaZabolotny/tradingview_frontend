import * as types from "../types/types";

export const signupUser = (values, successAction) => {
  return {
    type: types.SIGNUP_USER,
    values,
    successAction,
  };
};

export const userSignedUP = (user) => {
  return {
    type: types.USER_SIGNED_UP,
    user,
  };
};

export const signupError = () => {
  return {
    type: types.SIGNUP_ERROR,
  };
};
