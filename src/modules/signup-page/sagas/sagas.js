import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { signupError, userSignedUP } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ values, successAction }) {
  const service = new Service();
  const result = {};
  result.login = values.login;
  result.email = values.email;
  result.password = values.password;
  try {
    const user = yield call(service.register, result);
    yield put(userSignedUP(user));
    successAction();
  } catch (error) {
    yield put(signupError(error));
  }
}

export function* signupWatcher() {
  yield takeLatest(actionTypes.SIGNUP_USER, sagaWorker);
}
