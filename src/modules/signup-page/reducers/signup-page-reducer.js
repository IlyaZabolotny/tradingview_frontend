import * as actionTypes from "../types/types";

const initialState = {
  user: null,
  errors: false,
};

const signupPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USER_SIGNED_UP:
      return {
        ...state,
        user: action.user,
      };
    case actionTypes.SIGNUP_ERROR:
      return {
        ...state,
        errors: true,
      };
    default:
      return state;
  }
};

export default signupPageReducer;
