import React from "react";
import { Link, useHistory } from "react-router-dom";
import { Alert, Button, Col, Form, Input, Row } from "antd";
import styled from "styled-components";
import TradingviewCarousel from "../../../components/tradingview-carousel";

const validateMessages = {
  required: "",
  types: {
    email: "Invalid email address",
  },
};

const SignupPage = ({ signup, errors }) => {
  const history = useHistory();

  return (
    <Row>
      <Col span={12} align="center">
        <StyledTitle align="center">Register to TradingView </StyledTitle>
        <StyledText>
          Already have an account?
          <Link to="/"> Log in here</Link>
        </StyledText>
        <StyledForm
          name="registration"
          onFinish={(values) => signup(values, () => history.push("/"))}
          validateMessages={validateMessages}
          fields={["login", "email", "password"]}
        >
          <StyledFormItem
            name="login"
            rules={[
              {
                required: true,
              },
            ]}
            hasFeedback
          >
            <StyledInput placeholder="Your login" />
          </StyledFormItem>
          <StyledFormItem
            name="email"
            rules={[
              {
                required: true,
                type: "email",
              },
            ]}
            hasFeedback
          >
            <StyledInput placeholder="Your email" />
          </StyledFormItem>

          <StyledFormItem
            name="password"
            rules={[
              {
                required: true,
              },
              {
                min: 5,
                message: "The password must be more than 4 characters long",
              },
            ]}
            hasFeedback
          >
            <StyledInput placeholder="Your password" />
          </StyledFormItem>

          <StyledFormItem
            name="confirm"
            rules={[
              {
                required: true,
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if ((!value, getFieldValue("password") === value)) {
                    return Promise.resolve();
                  }
                  // eslint-disable-next-line prefer-promise-reject-errors
                  return Promise.reject("Passwords do not match");
                },
              }),
            ]}
            dependencies={["password"]}
            hasFeedback
          >
            <StyledInput placeholder="Confirm password" />
          </StyledFormItem>
          <StyledFormItem>
            <StyledButton
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Register
            </StyledButton>
          </StyledFormItem>
          {errors && (
            <StyledAlert
              message="The user has already an account"
              type="error"
              showIcon
            />
          )}
        </StyledForm>
      </Col>
      <Col span={12}>
        <TradingviewCarousel />
      </Col>
    </Row>
  );
};

const StyledTitle = styled.h1`
  margin-top: 210px;
  margin-bottom: 5px;
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 500;
  font-size: 40px;
  color: #585e71;
`;

const StyledText = styled.p`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 100;
  font-size: 18px;
  line-height: 22px;
`;

const StyledForm = styled(Form)`
  max-width: 440px;
  margin-top: 50px;
`;

const StyledFormItem = styled(Form.Item)`
  padding-bottom: 7px;
`;

const StyledInput = styled(Input)`
  height: 50px;
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 18px;
  font-style: normal;
`;

const StyledButton = styled(Button)`
  width: 100%;
  height: 60px;
  background: #3696f0;
  border-radius: 10px;
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 600;
  font-size: 25px;
  line-height: 37px;
  margin-top: 20px;
`;

const StyledAlert = styled(Alert)`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 300;
  font-size: 18px;
`;

export default SignupPage;
