import { connect } from "react-redux";
import { signupUser } from "../actions";
import SignupPage from "../components/signup-page";

const mapStateToProps = (state) => {
  return {
    errors: state.signupPage.errors,
  };
};

const mapDispatchToProps = (dispatch) => ({
  signup: (values, successAction) => {
    dispatch(signupUser(values, successAction));
  },
});

const SignupPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SignupPage);

export default SignupPageContainer;
