import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import * as actionTypes from "../types/types";
import { loadStockData } from "../actions";

function* sagaWorker({ symbol }) {
  const service = new Service();
  try {
    const pricePayload = yield call(service.getStocksData, symbol);
    const volumePayload = yield call(service.getStocksVolumeData, symbol);
    yield put(loadStockData(symbol, pricePayload.data, volumePayload.data));
  } catch (error) {
    console.log(error);
  }
}

export function* stockChartTableWatcher() {
  yield takeLatest(actionTypes.FETCH_STOCK_DATA_REQUEST, sagaWorker);
}
