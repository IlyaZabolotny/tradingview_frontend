import * as actionTypes from "../types/types";

const initialState = {
  priceData: [],
  volumeData: [],
  loading: true,
};

const stockChartTableReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_STOCK_DATA_REQUEST:
      return {
        ...state,
        priceData: [],
        volumeData: [],
        loading: true,
      };
    case actionTypes.FETCH_STOCK_DATA_SUCCESS:
      return {
        ...state,
        priceData: action.priceData,
        volumeData: action.volumeData,
        loading: false,
      };
    default:
      return state;
  }
};

export default stockChartTableReducer;
