import * as types from "../types/types";

export const requestStockData = (symbol) => {
  return {
    type: types.FETCH_STOCK_DATA_REQUEST,
    symbol,
  };
};

export const loadStockData = (symbol, priceData, volumeData) => {
  return {
    type: types.FETCH_STOCK_DATA_SUCCESS,
    symbol,
    priceData,
    volumeData,
  };
};
