import React, { useEffect, useMemo } from "react";
import styled from "styled-components";
import Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";
import { Spin } from "antd";

require("highcharts/modules/drag-panes")(Highcharts);
require("highcharts/modules/exporting")(Highcharts);
require("highcharts/modules/price-indicator")(Highcharts);
require("highcharts/indicators/indicators")(Highcharts);
require("highcharts/indicators/ema")(Highcharts);
require("highcharts/indicators/macd")(Highcharts);
require("highcharts/indicators/atr")(Highcharts);
require("highcharts/indicators/wma")(Highcharts);

const StockChartTable = ({
  loading,
  symbol,
  priceData,
  volumeData,
  fetchStocksData,
}) => {
  useEffect(() => {
    fetchStocksData(symbol);
  }, []);

  const stockOptions = useMemo(
    () => ({
      yAxis: [
        {
          labels: {
            align: "right",
            x: -3,
          },
          title: {
            text: symbol,
          },
          height: "50%",
          lineWidth: 2,
          resize: {
            enabled: true,
          },
        },
        {
          labels: {
            align: "right",
            x: -3,
          },
          title: {
            text: "Volume",
          },
          height: "25%",
          top: "50%",
          offset: 0,
          resize: {
            enabled: true,
          },
        },
        {
          labels: {
            align: "right",
            x: -3,
          },
          title: {
            text: "Indicators",
          },
          height: "25%",
          top: "75%",
          offset: 0,
          resize: {
            enabled: true,
          },
        },
      ],
      title: {
        text: `${symbol} STOCK PRICE`,
      },
      legend: {
        enabled: true,
      },
      plotOptions: {
        series: {
          showInLegend: true,
        },
      },
      tooltip: {
        split: true,
        enabledIndicators: true,
      },
      chart: {
        height: 610,
        style: {
          fontFamily: "'Noto Sans', serif",
        },
      },
      series: [
        {
          data: priceData,
          type: "candlestick",
          name: symbol,
          id: symbol,
          color: "#3faa9e",
          title: {
            text: symbol,
          },
          tooltip: {
            valueDecimals: 2,
          },
          fillColor: {
            linearGradient: {
              x1: 0,
              y1: 0,
              x2: 0,
              y2: 1,
            },
            stops: [
              [0, "#3faa9e"],
              [
                1,
                Highcharts.color(Highcharts.getOptions().colors[0])
                  .setOpacity(0)
                  .get("rgba"),
              ],
            ],
          },
          threshold: null,
        },
        {
          type: "column",
          id: "volume",
          name: "Volume",
          data: volumeData,
          color: Highcharts.getOptions().colors[1],
          yAxis: 1,
        },
        {
          type: "ema",
          yAxis: 2,
          linkedTo: symbol,
        },
        {
          type: "wma",
          yAxis: 2,
          linkedTo: symbol,
        },
        {
          type: "macd",
          yAxis: 2,
          linkedTo: symbol,
        },
      ],
      rangeSelector: {
        verticalAlign: "top",
        x: 0,
        y: 0,
        buttons: [
          {
            type: "1m",
            text: "1m",
            count: 1,
          },
        ],
      },
    }),
    [symbol, priceData, volumeData]
  );

  return (
    <div>
      {loading ? (
        <SpinWrapper>
          <Spin tip="Loading stock chart..." />
        </SpinWrapper>
      ) : (
        <ChartsWrapper>
          <HighchartsReact
            options={stockOptions}
            highcharts={Highcharts}
            constructorType="stockChart"
          />
        </ChartsWrapper>
      )}
    </div>
  );
};

const ChartsWrapper = styled.div`
  padding-top: 20px;
`;

const SpinWrapper = styled.div`
  min-height: 610px;
  padding-top: 220px;
  padding-left: 750px;
`;

export default StockChartTable;
