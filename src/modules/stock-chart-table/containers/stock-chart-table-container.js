import { connect } from "react-redux";
import { requestStockData } from "../actions";
import StockChartTable from "../components/stock-chart-table";

const mapStateToProps = (state) => {
  const { stocksData } = state;
  return {
    priceData: stocksData.priceData,
    volumeData: stocksData.volumeData,
    loading: stocksData.loading,
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchStocksData: (symbol) => {
    dispatch(requestStockData(symbol));
  },
});

const StockChartTableContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(StockChartTable);

export default StockChartTableContainer;
