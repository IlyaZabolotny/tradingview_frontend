import * as actionTypes from "../types/types";

const initialState = {
  profile: [],
};

const profileBioReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_PROFILE_SUCCESS:
      return {
        ...state,
        profile: action.payload,
      };
    default:
      return state;
  }
};

export default profileBioReducer;
