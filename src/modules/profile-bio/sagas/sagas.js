import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { loadProfile } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ userId }) {
  const service = new Service();
  try {
    const payload = yield call(service.getProfile, userId);
    yield put(loadProfile(payload.data));
  } catch (error) {
    console.log(error);
  }
}

export function* profileBioWatcher() {
  yield takeLatest(actionTypes.FETCH_PROFILE_REQUEST, sagaWorker);
}
