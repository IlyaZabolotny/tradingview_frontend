import * as types from "../types/types";

export const loadProfile = (profile) => {
  return {
    type: types.FETCH_PROFILE_SUCCESS,
    payload: profile,
  };
};

export const requestProfile = (userId) => {
  return {
    type: types.FETCH_PROFILE_REQUEST,
    userId,
  };
};
