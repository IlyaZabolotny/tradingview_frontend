import { connect } from "react-redux";
import { requestProfile } from "../actions";
import ProfileBio from "../components/profile-bio";

const mapStateToProps = (state) => {
  return {
    profile: state.profile.profile,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getProfile: (userId) => {
      dispatch(requestProfile(userId));
    },
  };
};

const ProfileBioContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileBio);

export default ProfileBioContainer;
