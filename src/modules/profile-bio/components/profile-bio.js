import React, { useEffect, useState } from "react";
import {
  BarChartOutlined,
  CarryOutFilled,
  ClockCircleFilled,
  FormOutlined,
  LikeOutlined,
  TeamOutlined,
} from "@ant-design/icons";
import { Avatar, Statistic, Typography } from "antd";
import moment from "moment";
import styled from "styled-components";
import Cookies from "universal-cookie";
import UserSettingsContainer from "../../user-settings/containers/user-settings-container";

const ProfileBio = ({ anotherUserId, profile, getProfile }) => {
  const [userId, setUserId] = useState(null);
  const [visibleSettings, setVisibleSettings] = useState(false);
  const [anotherUser, setAnotherUser] = useState(false);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setUserId(cookies.get("user_id"));
    }
  }, []);

  useEffect(() => {
    if (userId) {
      if (userId !== anotherUserId) {
        getProfile(anotherUserId);
        setAnotherUser(true);
      } else {
        getProfile(userId);
      }
    }
  }, [userId]);
  return (
    <Wrapper>
      <StyledAvatar
        shape="square"
        src={`data:image/jpeg;base64,${profile.avatar}`}
      >
        {(profile.login || "").charAt(0).toUpperCase()}
      </StyledAvatar>
      <div>
        <ProfileName>
          <StyledUserName>{profile.login}</StyledUserName>
          {!anotherUser && (
            <div>
              <StyledFormOutlined
                style={{ fontSize: "20px" }}
                onClick={() => {
                  setVisibleSettings(true);
                }}
              />
              <UserSettingsContainer
                visible={visibleSettings}
                onCancel={() => setVisibleSettings(false)}
                setVisible={setVisibleSettings}
                userData={profile}
              />
            </div>
          )}
        </ProfileName>
        <ProfileInfo>
          {!anotherUser && (
            <StatusWrapper>
              <StyledClockCircleFilled />
              <Text> Online </Text>
            </StatusWrapper>
          )}
          <RegistrationWrapper>
            <StyledCarryOutFilled />
            <Text>
              Registration date:{" "}
              {moment(profile.registrationDate).format("Do MMM  YYYY")}
            </Text>
          </RegistrationWrapper>
        </ProfileInfo>
        <ProfileSocials>
          <StyledStatistic
            title="Ideas"
            value={profile.ideas}
            groupSeparator="."
            prefix={<BarChartOutlined style={{ fontSize: "15px" }} />}
          />
          <StyledStatistic
            title="Likes"
            value={profile.likes}
            groupSeparator="."
            prefix={<LikeOutlined style={{ fontSize: "15px" }} />}
          />
          <StyledStatistic
            title="Followers"
            value={profile.followers}
            groupSeparator="."
            prefix={<TeamOutlined style={{ fontSize: "15px" }} />}
          />
        </ProfileSocials>
      </div>
    </Wrapper>
  );
};

const { Text } = Typography;

const StyledAvatar = styled(Avatar)`
  width: 150px;
  height: 150px;
  border-radius: 3px;
  background: #61699d;
  font-size: 100px;
  line-height: 130px;
`;

const Wrapper = styled.div`
  padding: 20px 0;
  margin: 0 64px;
  display: flex;
  border-bottom: 1px solid #e0e3eb;
  border-top: 1px solid #e0e3eb;
`;

const StyledUserName = styled.h1`
  margin: 0 32px 10px;
  font-size: 32px;
  font-family: "Noto Sans", serif;
  font-weight: 800;
`;

const ProfileInfo = styled.div`
  display: flex;
  margin: 0 32px 15px;
  font-family: "Noto Sans", serif;
  font-size: 12px;
`;

const ProfileName = styled.div`
  display: flex;
`;

const StyledFormOutlined = styled(FormOutlined)`
  margin-top: 18px;
  margin-left: -20px;
`;

const ProfileSocials = styled.div`
  margin: 0 32px;
  display: flex;
`;

const StyledStatistic = styled(Statistic)`
  font-family: "Noto Sans", serif;
  margin-right: 50px;
  .ant-statistic-title {
    font-size: 15px;
    color: #787b86;
    font-weight: 300;
    margin: 0;
  }
  .ant-statistic-content-value-int {
    font-family: "Noto Sans", serif;
    font-size: 18px;
    font-weight: 400;
  }
`;

const StatusWrapper = styled.div`
  margin-right: 20px;
  color: #26a69a;
`;

const RegistrationWrapper = styled.div`
  color: #787b86;
`;

const StyledClockCircleFilled = styled(ClockCircleFilled)`
  padding-right: 10px;
`;

const StyledCarryOutFilled = styled(CarryOutFilled)`
  padding-right: 10px;
`;

export default ProfileBio;
