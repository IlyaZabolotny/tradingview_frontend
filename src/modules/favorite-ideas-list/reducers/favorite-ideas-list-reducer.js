import * as actionTypes from "../types/types";

const initialState = {
  favoriteIdeas: [],
  pageNumber: 0,
  total: 1,
};

const favoriteIdeasListReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_FAVORITE_IDEAS_SUCCESS:
      return {
        ...state,
        favoriteIdeas: action.favoriteIdeas,
        pageNumber: action.pageNumber,
        total: action.total,
      };
    case actionTypes.SET_LIKE_SUCCESS_FAVORITE:
      return {
        ...state,
      };
    case actionTypes.REMOVE_LIKE_SUCCESS_FAVORITE:
      return {
        ...state,
      };
    case actionTypes.SET_FAVORITE_IDEA_SUCCESS_FAVORITE:
      return {
        ...state,
      };
    case actionTypes.REMOVE_FAVORITE_IDEA_SUCCESS_FAVORITE:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default favoriteIdeasListReducer;
