import * as types from "../types/types";

export const loadFavoriteIdeas = (
  favoriteIdeas,
  pageNumber,
  total,
  keyword
) => {
  return {
    type: types.FETCH_FAVORITE_IDEAS_SUCCESS,
    favoriteIdeas,
    pageNumber,
    total,
    keyword,
  };
};

export const requestFavoriteIdeas = (userId, pageNumber, keyword) => {
  return {
    type: types.FETCH_FAVORITE_IDEAS_REQUEST,
    userId,
    pageNumber,
    keyword,
  };
};

export const setLikeRequest = (userId, ideaId, pageNumber, keyword) => {
  return {
    type: types.SET_LIKE_REQUEST_FAVORITE,
    userId,
    ideaId,
    pageNumber,
    keyword,
  };
};

export const setLikeSuccess = () => {
  return {
    type: types.SET_LIKE_SUCCESS_FAVORITE,
  };
};

export const removeLikeRequest = (userId, ideaId, pageNumber, keyword) => {
  return {
    type: types.REMOVE_LIKE_REQUEST_FAVORITE,
    userId,
    ideaId,
    pageNumber,
    keyword,
  };
};

export const removeLikeSuccess = () => {
  return {
    type: types.REMOVE_LIKE_SUCCESS_FAVORITE,
  };
};
export const setFavoriteIdeaRequest = (userId, ideaId, pageNumber, keyword) => {
  return {
    type: types.SET_FAVORITE_IDEA_REQUEST_FAVORITE,
    userId,
    ideaId,
    pageNumber,
    keyword,
  };
};

export const setFavoriteIdeaSuccess = () => {
  return {
    type: types.SET_FAVORITE_IDEA_SUCCESS_FAVORITE,
  };
};

export const removeFavoriteIdeaRequest = (
  userId,
  ideaId,
  pageNumber,
  keyword
) => {
  return {
    type: types.REMOVE_FAVORITE_IDEA_REQUEST_FAVORITE,
    userId,
    ideaId,
    pageNumber,
    keyword,
  };
};

export const removeFavoriteIdeaSuccess = () => {
  return {
    type: types.REMOVE_FAVORITE_IDEA_SUCCESS_FAVORITE,
  };
};
