import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Input } from "antd";
import Cookies from "universal-cookie";
import ContentList from "../../../components/content-list";

const { Search } = Input;

const FavoriteIdeasList = ({
  getFavoriteIdeas,
  favoriteIdeas,
  total,
  setLike,
  removeLike,
  setFavoriteIdea,
  removeFavoriteIdea,
}) => {
  const [page, setPage] = useState(1);

  const [userId, setUserId] = useState(null);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setUserId(cookies.get("user_id"));
    }
  }, []);

  useEffect(() => {
    if (userId !== null) {
      getFavoriteIdeas(userId, 1, "");
    }
  }, [userId]);

  return (
    <div>
      <StyledSearch
        id="favorite-ideas-search"
        placeholder="Search ideas"
        allowClear
        onSearch={(value) => {
          getFavoriteIdeas(userId, 1, value);
          setPage(1);
        }}
      />
      <ContentList
        fetchData={getFavoriteIdeas}
        type="favorite-ideas"
        setLike={setLike}
        removeLike={removeLike}
        setFavoriteIdea={setFavoriteIdea}
        removeFavoriteIdea={removeFavoriteIdea}
        authorized
        userId={userId}
        data={favoriteIdeas}
        pageSize={2}
        pageNumber={page}
        total={total}
        onChange={(pageNumber) => {
          const searchValue = document.getElementById(
            "favorite-ideas-search"
          ).value;
          getFavoriteIdeas(userId, pageNumber, searchValue);
          setPage(pageNumber);
        }}
      />
    </div>
  );
};

const StyledSearch = styled(Search)`
  margin-bottom: 20px;
  width: 30%;
  font-family: "Noto Sans", serif;
  font-style: normal;
`;

export default FavoriteIdeasList;
