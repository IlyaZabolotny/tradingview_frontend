import { connect } from "react-redux";
import {
  removeFavoriteIdeaRequest,
  removeLikeRequest,
  requestFavoriteIdeas,
  setFavoriteIdeaRequest,
  setLikeRequest,
} from "../actions";
import FavoriteIdeasList from "../components/favorite-ideas-list";

const mapStateToProps = (state) => {
  const { favoriteIdeas } = state;
  return {
    favoriteIdeas: favoriteIdeas.favoriteIdeas,
    pageNumber: favoriteIdeas.pageNumber,
    total: favoriteIdeas.total,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getFavoriteIdeas: (userId, pageNumber, keyword) => {
      dispatch(requestFavoriteIdeas(userId, pageNumber, keyword));
    },
    setLike: (userId, ideaId, pageNumber, keyword) => {
      dispatch(setLikeRequest(userId, ideaId, pageNumber, keyword));
    },
    removeLike: (userId, ideaId, pageNumber, keyword) => {
      dispatch(removeLikeRequest(userId, ideaId, pageNumber, keyword));
    },
    setFavoriteIdea: (userId, ideaId, pageNumber, keyword) => {
      dispatch(setFavoriteIdeaRequest(userId, ideaId, pageNumber, keyword));
    },
    removeFavoriteIdea: (userId, ideaId, pageNumber, keyword) => {
      dispatch(removeFavoriteIdeaRequest(userId, ideaId, pageNumber, keyword));
    },
  };
};

const FavoriteIdeasListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FavoriteIdeasList);

export default FavoriteIdeasListContainer;
