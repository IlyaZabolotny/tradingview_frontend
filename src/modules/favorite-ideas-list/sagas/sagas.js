import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import {
  loadFavoriteIdeas,
  removeFavoriteIdeaSuccess,
  removeLikeSuccess,
  setFavoriteIdeaSuccess,
  setLikeSuccess,
} from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ userId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.id = userId;
    result.keyword = keyword;
    result.pageNumber = pageNumber - 1;
    result.size = 2;
    const payload = yield call(service.getFavoriteIdeas, result);
    yield put(
      loadFavoriteIdeas(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* likesWorker({ userId, ideaId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.ideaId = ideaId;
    yield call(service.setLike, result);
    yield put(setLikeSuccess());
    const updateResult = {};
    updateResult.id = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 2;
    const updatePayload = yield call(service.getFavoriteIdeas, updateResult);
    yield put(
      loadFavoriteIdeas(
        updatePayload.data.pageList,
        updatePayload.data.page,
        updatePayload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* removeLikeWorker({ userId, ideaId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.ideaId = ideaId;
    yield call(service.removeLike, result);
    yield put(removeLikeSuccess());
    const updateResult = {};
    updateResult.id = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 2;
    const updatePayload = yield call(service.getFavoriteIdeas, updateResult);
    yield put(
      loadFavoriteIdeas(
        updatePayload.data.pageList,
        updatePayload.data.page,
        updatePayload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* favoriteIdeaWorker({ userId, ideaId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.ideaId = ideaId;
    yield call(service.setFavoriteIdea, result);
    yield put(setFavoriteIdeaSuccess);
    const updateResult = {};
    updateResult.id = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 2;
    const updatePayload = yield call(service.getFavoriteIdeas, updateResult);
    yield put(
      loadFavoriteIdeas(
        updatePayload.data.pageList,
        updatePayload.data.page,
        updatePayload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

function* removeFavoriteIdeaWorker({ userId, ideaId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.ideaId = ideaId;
    yield call(service.removeFavoriteIdea, result);
    yield put(removeFavoriteIdeaSuccess());
    const updateResult = {};
    updateResult.id = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 2;
    const updatePayload = yield call(service.getFavoriteIdeas, updateResult);
    yield put(
      loadFavoriteIdeas(
        updatePayload.data.pageList,
        updatePayload.data.page,
        updatePayload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

export function* favoriteIdeasWatcher() {
  yield takeLatest(actionTypes.FETCH_FAVORITE_IDEAS_REQUEST, sagaWorker);
}

export function* ideasLikesWatcherFavorite() {
  yield takeLatest(actionTypes.SET_LIKE_REQUEST_FAVORITE, likesWorker);
}

export function* ideasRemoveLikeWatcherFavorite() {
  yield takeLatest(actionTypes.REMOVE_LIKE_REQUEST_FAVORITE, removeLikeWorker);
}

export function* addToFavoriteIdeasWatcherFavorite() {
  yield takeLatest(
    actionTypes.SET_FAVORITE_IDEA_REQUEST_FAVORITE,
    favoriteIdeaWorker
  );
}

export function* removeFromFavoriteIdeasWatcherFavorite() {
  yield takeLatest(
    actionTypes.REMOVE_FAVORITE_IDEA_REQUEST_FAVORITE,
    removeFavoriteIdeaWorker
  );
}
