import React from "react";
import { Link, useHistory } from "react-router-dom";
import { Alert, Button, Col, Form, Input, Row } from "antd";
import styled from "styled-components";
import TradingviewCarousel from "../../../components/tradingview-carousel";
// import { sendMessage } from "../../../services/web-socket-service";

const validateMessages = {
  required: "",
};

const LoginPage = ({ login, errors }) => {
  const history = useHistory();

  return (
    <Row>
      <Col span={12} align="center">
        <StyledTitle align="center">Log in to TradingView</StyledTitle>
        <StyledText>
          {/* eslint-disable-next-line react/no-unescaped-entities */}
          Don't have an account?
          <Link to="/signup"> Register here!</Link>
        </StyledText>
        <StyledForm
          name="normal_login"
          onFinish={(values) => {
            login(values, () => {
              history.push("/main");
            });
            // sendMessage("some message");
          }}
          validateMessages={validateMessages}
        >
          <StyledFormItem
            name="login"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <StyledInput name="login" placeholder="Your login" />
          </StyledFormItem>

          <StyledFormItem
            name="password"
            rules={[
              {
                required: true,
              },
            ]}
          >
            <StyledInput
              name="password"
              type="password"
              placeholder="Your password"
            />
          </StyledFormItem>

          <StyledFormItem>
            <StyledButton
              type="primary"
              htmlType="submit"
              className="login-form-button"
            >
              Log in
            </StyledButton>
          </StyledFormItem>
          {errors && (
            <StyledAlert
              message="Invalid email or password"
              type="error"
              showIcon
            />
          )}
        </StyledForm>
      </Col>
      <Col span={12}>
        <TradingviewCarousel />
      </Col>
    </Row>
  );
};

const StyledTitle = styled.h1`
  margin-top: 260px;
  margin-bottom: 5px;
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 500;
  font-weight: 300;
  font-size: 42px;
  color: #585e71;
`;

const StyledText = styled.p`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 100;
  font-size: 18px;
  line-height: 22px;
`;

const StyledForm = styled(Form)`
  max-width: 440px;
  margin-top: 50px;
`;

const StyledFormItem = styled(Form.Item)`
  padding-bottom: 7px;
`;

const StyledInput = styled(Input)`
  height: 50px;
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  font-size: 18px;
  font-style: normal;
`;

const StyledButton = styled(Button)`
  width: 100%;
  height: 60px;
  background: #3696f0;
  border-radius: 10px;
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 600;
  font-size: 25px;
  line-height: 37px;
  margin-top: 20px;
`;

const StyledAlert = styled(Alert)`
  font-family: "Noto Sans", serif;
  font-style: normal;
  font-weight: 300;
  font-size: 18px;
`;

export default LoginPage;
