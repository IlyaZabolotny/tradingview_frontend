import { connect } from "react-redux";
import { loginUser } from "../actions";
import LoginPage from "../components/login-page";

const mapStateToProps = (state) => {
  return {
    errors: state.loginPage.errors,
  };
};

const mapDispatchToProps = (dispatch) => ({
  login: (values, successAction) => {
    dispatch(loginUser(values, successAction));
  },
});

const LoginPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage);

export default LoginPageContainer;
