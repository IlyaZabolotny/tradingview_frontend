import { takeLatest, call, put } from "redux-saga/effects";
import Cookies from "universal-cookie";
import Service from "../../../services/service";
import { loginError, userLoggedIn } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ values, successAction }) {
  const service = new Service();
  const cookies = new Cookies();
  try {
    const user = yield call(service.login, values);
    yield put(
      userLoggedIn(
        user.data.id,
        user.data.login,
        user.data.token,
        user.data.email
      )
    );
    cookies.set("token", user.data.token, { maxAge: 3600 });
    cookies.set("user_id", user.data.id, { maxAge: 3600 });
    successAction();
  } catch (error) {
    yield put(loginError(error));
  }
}

export function* loginPageWatcher() {
  yield takeLatest(actionTypes.LOGIN_USER, sagaWorker);
}
