import * as types from "../types/types";

export const loginUser = (values, successAction) => {
  return {
    type: types.LOGIN_USER,
    values,
    successAction,
  };
};

export const userLoggedIn = (userId, login, token, email) => {
  return {
    type: types.USER_LOGGED_IN,
    userId,
    login,
    token,
    email,
  };
};

export const loginError = () => {
  return {
    type: types.LOGIN_ERROR,
  };
};
