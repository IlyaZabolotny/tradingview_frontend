import * as actionTypes from "../types/types";

const initialState = {
  userId: null,
  login: null,
  token: null,
  email: null,
  authorized: false,
  errors: false,
};

const loginPageReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.USER_LOGGED_IN:
      return {
        ...state,
        userId: action.userId,
        login: action.login,
        token: action.token,
        email: action.email,
        authorized: true,
      };
    case actionTypes.LOGIN_ERROR:
      return {
        ...state,
        errors: true,
      };
    default:
      return state;
  }
};

export default loginPageReducer;
