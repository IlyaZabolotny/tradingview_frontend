import { connect } from "react-redux";
import { requestData } from "../actions";
import FavoriteStocksTable from "../components/favorite-stocks-table";

const mapStateToProps = (state) => {
  const { favoriteStocks } = state;
  return {
    data: favoriteStocks.data,
    loading: favoriteStocks.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchData: (userId) => {
      dispatch(requestData(userId));
    },
  };
};

const FavoriteStocksTableContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FavoriteStocksTable);

export default FavoriteStocksTableContainer;
