import * as actionTypes from "../types/types";

const initialState = {
  data: [],
  loading: true,
};

const favoriteStocksTableReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_FAVORITE_STOCK_DATA_SUCCESS:
      return {
        ...state,
        data: action.data,
        loading: false,
      };
    case actionTypes.FETCH_FAVORITE_STOCK_DATA_REQUEST:
      return {
        ...state,
        data: [],
        loading: true,
      };
    default:
      return state;
  }
};

export default favoriteStocksTableReducer;
