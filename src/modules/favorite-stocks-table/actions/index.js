import * as types from "../types/types";

export const requestData = (userId) => {
  return {
    type: types.FETCH_FAVORITE_STOCK_DATA_REQUEST,
    userId,
  };
};

export const loadData = (data) => {
  return {
    type: types.FETCH_FAVORITE_STOCK_DATA_SUCCESS,
    data,
  };
};
