import React, { useEffect, useState } from "react";
import Cookies from "universal-cookie";
import FavoriteStockTab from "../../../components/tabs/favorite-stock-tab";

const FavoriteStocksTable = ({ fetchData, data, loading }) => {
  const [userId, setUserId] = useState(null);
  const [authorized, setAuthorized] = useState(false);

  useEffect(() => {
    const cookies = new Cookies();
    if (cookies.get("token") !== undefined) {
      setAuthorized(true);
      // eslint-disable-next-line radix
      setUserId(cookies.get("user_id"));
    }
  }, []);

  useEffect(() => {
    if (authorized) {
      fetchData(userId);
    }
  }, [authorized]);

  return <FavoriteStockTab data={data} loading={loading} />;
};

export default FavoriteStocksTable;
