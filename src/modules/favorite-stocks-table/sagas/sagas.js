import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { loadData } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ userId }) {
  const service = new Service();
  try {
    // eslint-disable-next-line no-debugger
    debugger;
    const payload = yield call(service.getFavoriteStocks, userId);
    yield put(loadData(payload.data));
  } catch (error) {
    console.log(error);
  }
}

export function* favoriteStocksWatcher() {
  yield takeLatest(actionTypes.FETCH_FAVORITE_STOCK_DATA_REQUEST, sagaWorker);
}
