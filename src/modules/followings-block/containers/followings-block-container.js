import { connect } from "react-redux";
import { deleteFollowingRequest } from "../actions";
import FollowingsBlock from "../components/followings-block";

const mapDispatchToProps = (dispatch) => {
  return {
    deleteFollowing: (userId, subscribedTo, pageNumber, keyword) => {
      // eslint-disable-next-line no-debugger
      debugger;
      dispatch(
        deleteFollowingRequest(userId, subscribedTo, pageNumber, keyword)
      );
    },
  };
};

const FollowingsBlockContainer = connect(
  null,
  mapDispatchToProps
)(FollowingsBlock);

export default FollowingsBlockContainer;
