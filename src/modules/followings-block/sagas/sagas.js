import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { deleteFollowingSuccess } from "../actions";
import * as actionTypes from "../types/types";
import { loadFollowings } from "../../followings-list/actions";
import { loadFollowers } from "../../followers-list/actions";

function* deleteFollowingWorker({ userId, subscribedTo, pageNumber, keyword }) {
  const service = new Service();
  try {
    // eslint-disable-next-line no-debugger
    debugger;
    const result = {};
    result.userId = userId;
    result.subscribedTo = subscribedTo;
    yield call(service.unfollow, result);
    yield put(deleteFollowingSuccess());
    const updateResult = {};
    updateResult.userId = userId;
    updateResult.keyword = keyword;
    updateResult.pageNumber = pageNumber - 1;
    updateResult.size = 5;
    const payload = yield call(service.getFollowings, updateResult);
    yield put(
      loadFollowings(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        keyword
      )
    );
    const updateResultFollowers = {};
    updateResultFollowers.userId = userId;
    updateResultFollowers.keyword = "";
    updateResultFollowers.pageNumber = 0;
    updateResultFollowers.size = 5;
    const updatePayload = yield call(
      service.getFollowers,
      updateResultFollowers
    );
    yield put(
      loadFollowers(
        updatePayload.data.pageList,
        updatePayload.data.page,
        updatePayload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}
export function* deleteFollowingWatcher() {
  yield takeLatest(actionTypes.DELETE_FOLLOWING_REQUEST, deleteFollowingWorker);
}
