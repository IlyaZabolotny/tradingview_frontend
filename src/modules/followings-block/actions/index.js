import * as types from "../types/types";

export const deleteFollowingSuccess = () => {
  return {
    type: types.DELETE_FOLLOWING_SUCCESS,
  };
};

export const deleteFollowingRequest = (
  userId,
  subscribedTo,
  pageNumber,
  keyword
) => {
  return {
    type: types.DELETE_FOLLOWING_REQUEST,
    userId,
    subscribedTo,
    pageNumber,
    keyword,
  };
};
