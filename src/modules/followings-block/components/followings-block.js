import React from "react";
import { Button } from "antd";
import { StockOutlined, UserOutlined } from "@ant-design/icons";
import styled from "styled-components";

const FollowingsBlock = ({
  users,
  ideas,
  deleteFollowing,
  pageNumber,
  userId,
  subscribedTo,
  anotherUser,
}) => {
  return (
    <Wrapper>
      <ItemWrapper>
        <UserOutlined style={{ color: "#C8C8C8" }} />
        <Text>{users}</Text>
      </ItemWrapper>
      <ItemWrapper>
        <StockOutlined style={{ color: "#C8C8C8" }} />
        <Text>{ideas}</Text>
      </ItemWrapper>
      {!anotherUser && (
        <ItemWrapper>
          <StyledButton
            type="primary"
            onClick={() => {
              // eslint-disable-next-line no-debugger
              debugger;
              const searchValue =
                document.getElementById("followings-list").value;
              deleteFollowing(userId, subscribedTo, pageNumber, searchValue);
            }}
          >
            Unfollow
          </StyledButton>
        </ItemWrapper>
      )}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  display: flex;
`;

const ItemWrapper = styled.div`
  margin: 0 20px;
  padding: 10px;
  width: 100%;
  display: flex;
`;

const Text = styled.div`
  padding-left: 10px;
  font-size: 16px;
  color: #c8c8c8;
`;

const StyledButton = styled(Button)`
  width: 150px;
  height: 40px;
  border-radius: 5px;
`;

export default FollowingsBlock;
