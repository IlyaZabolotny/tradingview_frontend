import * as actionTypes from "../types/types";

const initialState = {};

const followingsBlockReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.DELETE_FOLLOWING_SUCCESS:
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default followingsBlockReducer;
