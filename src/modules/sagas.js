import { all } from "redux-saga/effects";
import {
  addToFavoriteIdeasWatcher,
  ideasLikesWatcher,
  ideasRemoveLikeWatcher,
  ideasUnauthorizedWatcher,
  ideasWatcher,
  removeFromFavoriteIdeasWatcher,
} from "./ideas-list/sagas/sagas";
import {
  followUserWatcher,
  unfollowUserWatcher,
  usersAuthWatcher,
  usersWatcher,
} from "./users-list/sagas/sagas";
import { mainPageWatcher, signOutWatcher } from "./main-page/sagas/sagas";
import { loginPageWatcher } from "./login-page/sagas/sagas";
import { signupWatcher } from "./signup-page/sagas/sagas";
import { userPageWatcher } from "./user-page/sagas/sagas";
import { profileBioWatcher } from "./profile-bio/sagas/sagas";
import { userIdeasWatcher } from "./user-ideas-list/sagas/sagas";
import {
  addToFavoriteIdeasWatcherFavorite,
  favoriteIdeasWatcher,
  ideasLikesWatcherFavorite,
  ideasRemoveLikeWatcherFavorite,
  removeFromFavoriteIdeasWatcherFavorite,
} from "./favorite-ideas-list/sagas/sagas";
import { followersWatcher } from "./followers-list/sagas/sagas";
import { followingsWatcher } from "./followings-list/sagas/sagas";
import { createIdeaWatcher } from "./idea-form/sagas/sagas";
import { stocksWatcher } from "./stocks-table/sagas/sagas";
import {
  removeFavoriteStockWatcher,
  setFavoriteStockWatcher,
  stockPageWatcher,
  stockStateWatcher,
  stockUserWatcher,
} from "./stcok-page/sagas/sagas";
import { stockChartTableWatcher } from "./stock-chart-table/sagas/sagas";
import { marketPageWatcher } from "./market-page/sagas/sagas";
import { favoriteStocksWatcher } from "./favorite-stocks-table/sagas/sagas";
import { deleteFollowingWatcher } from "./followings-block/sagas/sagas";
import {
  addFollowingWatcher,
  deleteFollowingFollowersWatcher,
} from "./followers-block/sagas/sagas";
import { tradingViewHeaderWatcher } from "./main-trading-view-header/sagas/sagas";
import { editUserProfileWatcher } from "./user-settings/sagas/sagas";
import { fetchIdeaWatcherById } from "./idea-watcher-form/sagas/sagas";

export function* rootWatcher() {
  yield all([
    ideasWatcher(),
    usersWatcher(),
    mainPageWatcher(),
    loginPageWatcher(),
    signupWatcher(),
    userPageWatcher(),
    profileBioWatcher(),
    userIdeasWatcher(),
    favoriteIdeasWatcher(),
    followersWatcher(),
    followingsWatcher(),
    createIdeaWatcher(),
    stocksWatcher(),
    ideasLikesWatcher(),
    stockPageWatcher(),
    stockChartTableWatcher(),
    signOutWatcher(),
    ideasRemoveLikeWatcher(),
    addToFavoriteIdeasWatcher(),
    removeFromFavoriteIdeasWatcher(),
    ideasUnauthorizedWatcher(),
    setFavoriteStockWatcher(),
    stockStateWatcher(),
    removeFavoriteStockWatcher(),
    stockUserWatcher(),
    marketPageWatcher(),
    usersAuthWatcher(),
    followUserWatcher(),
    unfollowUserWatcher(),
    favoriteStocksWatcher(),
    deleteFollowingWatcher(),
    addFollowingWatcher(),
    ideasLikesWatcherFavorite(),
    ideasRemoveLikeWatcherFavorite(),
    addToFavoriteIdeasWatcherFavorite(),
    removeFromFavoriteIdeasWatcherFavorite(),
    tradingViewHeaderWatcher(),
    editUserProfileWatcher(),
    deleteFollowingFollowersWatcher(),
    fetchIdeaWatcherById(),
  ]);
}
