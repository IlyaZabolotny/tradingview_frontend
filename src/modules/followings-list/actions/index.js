import * as types from "../types/types";

export const loadFollowings = (followings, pageNumber, total, keyword) => {
  return {
    type: types.FETCH_FOLLOWINGS_SUCCESS,
    followings,
    pageNumber,
    total,
    keyword,
  };
};

export const requestFollowings = (userId, pageNumber, keyword) => {
  return {
    type: types.FETCH_FOLLOWINGS_REQUEST,
    userId,
    pageNumber,
    keyword,
  };
};
