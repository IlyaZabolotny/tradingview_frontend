import { connect } from "react-redux";
import { requestFollowings } from "../actions";
import FollowingsList from "../components/followings-list";

const mapStateToProps = (state) => {
  const { followings } = state;
  return {
    followings: followings.followings,
    pageNumber: followings.pageNumber,
    total: followings.total,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getFollowings: (userId, pageNumber, keyword) => {
      dispatch(requestFollowings(userId, pageNumber, keyword));
    },
  };
};

const FollowingsListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FollowingsList);

export default FollowingsListContainer;
