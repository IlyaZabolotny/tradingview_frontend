import { takeLatest, call, put } from "redux-saga/effects";
import Service from "../../../services/service";
import { loadFollowings } from "../actions";
import * as actionTypes from "../types/types";

function* sagaWorker({ userId, pageNumber, keyword }) {
  const service = new Service();
  try {
    const result = {};
    result.userId = userId;
    result.keyword = keyword;
    result.pageNumber = pageNumber - 1;
    result.size = 5;
    // eslint-disable-next-line no-debugger
    debugger;
    const payload = yield call(service.getFollowings, result);
    yield put(
      loadFollowings(
        payload.data.pageList,
        payload.data.page,
        payload.data.nrOfElements,
        keyword
      )
    );
  } catch (error) {
    console.log(error);
  }
}

export function* followingsWatcher() {
  yield takeLatest(actionTypes.FETCH_FOLLOWINGS_REQUEST, sagaWorker);
}
