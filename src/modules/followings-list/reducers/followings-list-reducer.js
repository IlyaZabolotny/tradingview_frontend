import * as actionTypes from "../types/types";

const initialState = {
  followings: [],
  pageNumber: 1,
  total: 1,
};

const followingsListReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FETCH_FOLLOWINGS_SUCCESS:
      return {
        ...state,
        followings: action.followings,
        pageNumber: action.pageNumber,
        total: action.total,
      };
    default:
      return state;
  }
};

export default followingsListReducer;
