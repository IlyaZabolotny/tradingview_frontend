import { combineReducers } from "redux";
import ideasListReducer from "./ideas-list/reducers/ideas-list-reducer";
import usersListReducer from "./users-list/reducers/users-list-reducer";
import mainPageReducer from "./main-page/reducers/main-page-reducer";
import loginPageReducer from "./login-page/reducers/login-page-reducer";
import signupPageReducer from "./signup-page/reducers/signup-page-reducer";
import userPageReducer from "./user-page/reducers/user-page-reducer";
import profileBioReducer from "./profile-bio/reducers/profile-bio-reducer";
import userIdeasListReducer from "./user-ideas-list/reducers/user-ideas-list-reducer";
import favoriteIdeasListReducer from "./favorite-ideas-list/reducers/favorite-ideas-list-reducer";
import followersListReducer from "./followers-list/reducers/followers-list-reducer";
import followingsListReducer from "./followings-list/reducers/followings-list-reducer";
import ideaFormReducer from "./idea-form/reducers/idea-form-reducer";
import stocksTableReducer from "./stocks-table/reducers/stocks-table-reducer";
import stockPageReducer from "./stcok-page/reducers/stock-page-reducer";
import stockChartTableReducer from "./stock-chart-table/reducers/stock-chart-table-reducer";
import marketPageReducer from "./market-page/reducers/market-page-reducer";
import favoriteStocksTableReducer from "./favorite-stocks-table/reducers/favorite-stocks-table-reducer";
import followingsBlockReducer from "./followings-block/reducers/followings-block-reducer";
import mainTradingViewHeaderReducer from "./main-trading-view-header/reducers/main-trading-view-header-reducer";
import userSettingsReducer from "./user-settings/reducers/user-settings-reducer";
import followersBlockReducer from "./followers-block/reducers/followers-block-reducer";
import ideaWatcherFormReducer from "./idea-watcher-form/reducers/idea-watcher-form-reducer";
import patchIdeaFormReducer from "./patch-idea-form/reducers/patch-idea-form-reducer";

const rootReducer = combineReducers({
  ideas: ideasListReducer,
  users: usersListReducer,
  mainPage: mainPageReducer,
  loginPage: loginPageReducer,
  signupPage: signupPageReducer,
  userPage: userPageReducer,
  profile: profileBioReducer,
  userIdeas: userIdeasListReducer,
  favoriteIdeas: favoriteIdeasListReducer,
  followers: followersListReducer,
  followings: followingsListReducer,
  ideaForm: ideaFormReducer,
  stocks: stocksTableReducer,
  stock: stockPageReducer,
  stocksData: stockChartTableReducer,
  marketPage: marketPageReducer,
  favoriteStocks: favoriteStocksTableReducer,
  followingsBlock: followingsBlockReducer,
  followersBlock: followersBlockReducer,
  header: mainTradingViewHeaderReducer,
  userSettings: userSettingsReducer,
  ideaWatcher: ideaWatcherFormReducer,
  patchIdea: patchIdeaFormReducer,
});

export default rootReducer;
