import * as axios from "axios";
import Cookies from "universal-cookie";

const cookies = new Cookies();

export default class Service {
  // eslint-disable-next-line class-methods-use-this
  async login(values) {
    // eslint-disable-next-line no-return-await
    return await axios.post(
      "http://localhost:8080/authorization/login",
      values
    );
  }

  // eslint-disable-next-line class-methods-use-this
  async logout(id) {
    // eslint-disable-next-line no-return-await
    return await axios.post(`http://localhost:8080/authorization/logout/${id}`);
  }

  // eslint-disable-next-line class-methods-use-this
  async register(values) {
    // eslint-disable-next-line no-return-await
    return await axios.post(
      "http://localhost:8080/authorization/register",
      values
    );
  }

  // eslint-disable-next-line class-methods-use-this
  async publish(values) {
    return axios({
      method: "post",
      url: "http://localhost:8080/ideas/add",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async patchUser(values) {
    return axios({
      method: "patch",
      url: "http://localhost:8080/users/update",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async patchIdea(values) {
    return axios({
      method: "patch",
      url: "http://localhost:8080/ideas/update",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getAuthorIdeas(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/ideas/author/list", {
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
      params,
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getAllIdeas(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/ideas", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getAllIdeasUnauthorized(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/ideas/unauthorized", params);
  }

  // eslint-disable-next-line class-methods-use-this
  async searchStock(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/stocks/search", params);
  }

  // eslint-disable-next-line class-methods-use-this
  async getUser(id) {
    return axios({
      method: "get",
      url: `http://localhost:8080/users/${id}`,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getUsersUnauthorized(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/users/unauthorized", params);
  }

  // eslint-disable-next-line class-methods-use-this
  async getUsers(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/users", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getProfile(id) {
    return axios({
      method: "get",
      url: `http://localhost:8080/users/profile/${id}`,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getFavoriteIdeas(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/ideas/favorites", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getFollowers(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/users/followers", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getFollowings(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/users/followings", {
      headers: {
        Authorization: `Bearer_${cookies.get("token")}`,
      },
      params,
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getStocks(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/stocks", params);
  }

  // eslint-disable-next-line class-methods-use-this
  async getFavoriteStocks(id) {
    return axios({
      method: "get",
      url: `http://localhost:8080/stocks/favorite/${id}`,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async getIdea(id) {
    // eslint-disable-next-line no-return-await
    return await axios.get(`http://localhost:8080/ideas/${id}`);
  }

  // eslint-disable-next-line class-methods-use-this
  async getStock(symbol) {
    // eslint-disable-next-line no-return-await
    return await axios.get(`http://localhost:8080/stocks/${symbol}`);
  }

  // eslint-disable-next-line class-methods-use-this
  async getStocksData(symbol) {
    // eslint-disable-next-line no-return-await
    return await axios.get(
      `http://localhost:8080/stocks/time-series/${symbol}`
    );
  }

  // eslint-disable-next-line class-methods-use-this
  async getStocksVolumeData(symbol) {
    // eslint-disable-next-line no-return-await
    return await axios.get(
      `http://localhost:8080/stocks/time-series-volume/${symbol}`
    );
  }

  // eslint-disable-next-line class-methods-use-this
  async getMarketActives() {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/markets/actives");
  }

  // eslint-disable-next-line class-methods-use-this
  async getMarketGainers() {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/markets/gainers");
  }

  // eslint-disable-next-line class-methods-use-this
  async getMarketLosers() {
    // eslint-disable-next-line no-return-await
    return await axios.get("http://localhost:8080/markets/losers");
  }

  // eslint-disable-next-line class-methods-use-this
  async checkFavoriteStock(params) {
    // eslint-disable-next-line no-return-await
    return await axios.get(
      "http://localhost:8080/favorite-stocks/searchByUserIdAndSymbol",
      {
        headers: {
          Authorization: `Bearer_${cookies.get("token")}`,
        },
        params,
      }
    );
  }

  // eslint-disable-next-line class-methods-use-this
  async addFavoriteStock(values) {
    return axios({
      method: "post",
      url: "http://localhost:8080/favorite-stocks/add",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async removeFavoriteStock(values) {
    return axios({
      method: "delete",
      url: "http://localhost:8080/favorite-stocks/delete",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async follow(values) {
    return axios({
      method: "post",
      url: "http://localhost:8080/subscriptions/add",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async unfollow(values) {
    return axios({
      method: "delete",
      url: "http://localhost:8080/subscriptions/delete",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async setLike(values) {
    return axios({
      method: "post",
      url: "http://localhost:8080/likes/add",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async removeLike(values) {
    return axios({
      method: "delete",
      url: "http://localhost:8080/likes/delete",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async setFavoriteIdea(values) {
    return axios({
      method: "post",
      url: "http://localhost:8080/favorite-ideas/add",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }

  // eslint-disable-next-line class-methods-use-this
  async removeFavoriteIdea(values) {
    return axios({
      method: "delete",
      url: "http://localhost:8080/favorite-ideas/delete",
      data: values,
      headers: { Authorization: `Bearer_${cookies.get("token")}` },
    });
  }
}
