import SockJS from "sockjs-client";
import { Stomp } from "@stomp/stompjs";
import { notification } from "antd";
import Cookies from "universal-cookie";

let stompClient = null;

export function connect() {
  const cookies = new Cookies();
  const socket = new SockJS("http://localhost:8080/websocket");
  stompClient = Stomp.over(socket);
  stompClient.connect({}, (frame) => {
    console.log(`connected: ${frame}`);
    stompClient.subscribe(
      `/topic/notifications/${cookies.get("user_id")}`,
      (message) => {
        console.log(message.body);
        notification.open({
          message: message.body,
          duration: 5,
        });
      }
    );
  });
}

export function disconnect() {
  if (stompClient !== null) {
    stompClient.disconnect();
  }
}
